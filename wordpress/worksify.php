<?php
/*open graph*/
function laovu_opengraph() {
 
 $img_src = get_home_url() . '/wp-content/uploads/2021/08/bannerhome.png';
 $excerpt = get_the_title();
  if(get_post_type()=='job_listing' && get_post_meta( get_the_ID(), '_company_name', true )!="")
 {
    $excerpt = get_post( get_post_meta( get_the_ID(), '_company_name', true ) )->post_title.' | '.$excerpt;
 }
?>
 <meta property="og:image" content="<?php echo $img_src; ?>"/>
 <meta property="og:description" content="<?php echo $excerpt; ?>"/>
<?php

}
add_action('wp_head', 'laovu_opengraph', 5);


/*auto preview*/
add_filter( 'ppp_nonce_life', 'my_nonce_life' );
function my_nonce_life() {
    return 60 * 60 * 24 * 999;
}
function nonce_tick() {
        $nonce_life = apply_filters( 'ppp_nonce_life', 2 * DAY_IN_SECONDS ); // 2 days.

        return ceil( time() / ( $nonce_life / 2 ) );
    }
   function create_nonce( $action = -1 ) {
        $i = nonce_tick();

        return substr( wp_hash( $i . $action, 'nonce' ), -12, 10 );
    }
  function get_preview_link( $post ) {
        if ( 'page' === $post->post_type ) {
            $args = array(
                'page_id' => $post->ID,
            );
        } elseif ( 'post' === $post->post_type ) {
            $args = array(
                'p' => $post->ID,
            );
        } else {
            $args = array(
                'p'         => $post->ID,
                'post_type' => $post->post_type,
            );
        }
        $args['preview'] = true;
        $args['_ppp']    = create_nonce( 'public_post_preview_' . $post->ID );

        $link = add_query_arg( $args, home_url( '/' ) ); 
        return apply_filters( 'ppp_preview_link', $link, $post->ID, $post );
    }
 function auto_preview() {
  $post=get_posts(array('post_type'=>'job_listing','post_status'=> array('pending','expired','draft'),'nopaging' => true));
  $id_posts=array();
  foreach ($post as $value => $val) {
    $id_posts[]=$val->ID;
  }
  if(get_option('public_post_preview')!=$id_posts){
  update_option('public_post_preview',$id_posts);
 }
 }
 add_action('admin_init','auto_preview');

function direct_error_page()
{ 
if(!is_user_logged_in() && isset($_GET['_ppp'])){ ?>
<script type="text/javascript">
  jQuery(function($){
  window.location.href="<?=get_home_url()?>/error/";
  })
</script>
<?php }
}
add_action('wp_head','direct_error_page');

/*contact*/
function scroll_contact()
{ ?>
<script type="text/javascript">
  jQuery(function($){
    $('li.menu-item.menu-item-type-custom.menu-item-object-custom a').click(function(){
      setTimeout($('body').click(),500);
    });
    if($('.bookmark-details p:first-child')){$('.bookmark-details p:first-child').remove();}
  })
</script>
<?php }
add_action('wp_head','scroll_contact');

/*auto thêm company vào oder name*/
function auto_oder_company()
{
$args = array(
'post_type'=>'company',
'post_status'=>'any',
'nopaging' => true,);
$post_query = new WP_Query( $args );
            while ( $post_query->have_posts() ) 
            { 
              $post_query->the_post();
              $post_id=get_the_ID();
              $post = get_post($post_id); 
              $old_term=wp_get_post_terms( $post_id, 'oder_name' , array( 'fields' => 'ids' ));
              if(count($old_term)==0)
              {
              $slug = $post->post_name;
              $slug = substr($slug, 0, 1);
              $term_id=array(get_term_by('slug', $slug , 'oder_name')->term_id);
              $term_id[]=232;
              wp_set_post_terms( $post_id, $term_id , 'oder_name' );
              }
              elseif(count($old_term)>0)
              {
              $update=0;
              foreach (wp_get_post_terms( $post_id, 'oder_name' , array( 'fields' => 'ids' )) as $value ) 
              {
                if($value==232){$update++;}
              }
              if($update==0)
                {
                  $term_id=$old_term;
                  $term_id[]=232;
                  wp_set_post_terms( $post_id, $term_id , 'oder_name' );
                }
              }
            }
wp_reset_query();
}
add_action('admin_init','auto_oder_company');

/*auto thêm job vào category all jobs*/
function auto_all_jobs()
{
 $args = array(
'post_type'=>'job_listing',
'post_status'=> array('any','expired'),
'nopaging' => true);
$post_query = new WP_Query( $args );
            while ( $post_query->have_posts() ) { 
              $post_query->the_post();
              $post_id=get_the_ID();
                $cate_name=wp_get_post_terms( $post_id, 'job_listing_category', array( 'fields' => 'ids' ) );
                 $term_id=$cate_name;
                 $add=1;
                 foreach ($term_id as $value ) {
                   if($value==228){
                    $add=0;
                   }
                 }
                 if($add==1){ $term_id[]=228;wp_set_post_terms( $post_id, $term_id , 'job_listing_category' );}
                 }
wp_reset_query();
}
add_action('admin_init','auto_all_jobs');

/*phân quyền media cho employer*/
add_action('pre_get_posts','ml_restrict_media_library');
function ml_restrict_media_library( $wp_query_obj ) {
  $user = wp_get_current_user();
       $user_roles=$user->roles;
    global $current_user, $pagenow;
    if( !is_a( $current_user, 'WP_User') )
    return;
    if( 'admin-ajax.php' != $pagenow || $_REQUEST['action'] != 'query-attachments' )
    return;
    if( !current_user_can('manage_media_library') && $user_roles[0]=="employer")
    $wp_query_obj->set('author', $current_user->ID );
    return;
}
if ( current_user_can('employer') && !current_user_can('upload_files') )
    add_action('admin_init', 'allow_contributor_uploads');
function allow_contributor_uploads() {
    $contributor = get_role('employer');
    $contributor->add_cap('upload_files');
}
add_action ('wp_enqueue_scripts', 'child_theme_script');
function child_theme_script () {
    wp_enqueue_media();
    }

 /*xóa admin bar nếu kh phải admin*/
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}

/*lấy id ảnh từ url*/
function pn_get_attachment_id_from_url( $attachment_url = '' ) {
  global $wpdb;
  $attachment_id = false;
  
  // If there is no url, return.
  if ( '' == $attachment_url )
    return;
  
  // Get the upload directory paths
  $upload_dir_paths = wp_upload_dir();
  
  // Make sure the upload path base directory exists in the attachment URL, to verify that we're working with a media library image
  if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) ) {
  
    // If this is the URL of an auto-generated thumbnail, get the URL of the original image
    $attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );
  
    // Remove the upload path base directory from the attachment URL
    $attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );
  
    // Finally, run a custom database query to get the attachment ID from the modified attachment URL
    $attachment_id = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url ) );
  }
  return $attachment_id;
}

/*xóa thẻ html từ nội dung text*/
function strip_tags_content($string) { 
    $string = preg_replace ('/<[^>]*>/', ' ', $string); 
    $string = str_replace("\r", '', $string);
    $string = str_replace("\n", ' ', $string);
    $string = str_replace("\t", ' ', $string);
    $string = trim(preg_replace('/ {2,}/', ' ', $string));
    return $string; 
} 

/*chuyển đổi thời gian*/
function ash_relative_time($post_id) { 
$date_type=get_option("date_format");
$post_date = get_the_time('U',$post_id);
$delta = time() - $post_date;
if ( $delta < 60 ) {
    echo 'Just Now';
}
elseif ($delta > 60 && $delta < 120){
    echo '1 minute ago';
}
elseif ($delta > 120 && $delta < (60*60)){
    echo strval(round(($delta/60),0)), ' minutes ago';
}
elseif ($delta > (60*60) && $delta < (120*60)){
    echo 'About an hour ago';
}
elseif ($delta > (120*60) && $delta < (24*60*60)){
    echo strval(round(($delta/3600),0)), ' hours ago';
}
else {
    echo get_the_time($date_type,$post_id);
}}

/*job list home*/
if ( ! function_exists( 'home_job_list' ) ) {
function home_job_list($args,$content) {
    if(!isset($args['numb_show'])){$numb=5;} else {$numb=$args['numb_show'];}
    if(!isset($args['numb_count'])){$numb_count=10;} else {$numb_count=$args['numb_count'];}?>
  <div class="wrap_ajax_home_job_list">
    <div class="wrap_ajax_job_list desktop">
<?php
$job_id=array();
$post_query=null;
$args = array(
'post_type'=>'job_listing',
'post_status'=>'publish',
'posts_per_page' => $numb_count,
'meta_key' => '_featured',                  
'meta_value' => '1',               
'orderby' => 'modified',
'order' => 'DESC');
$post_query = new WP_Query( $args );
      if($post_query->have_posts()):
            while ( $post_query->have_posts() ) { ?>
            <?php
            $post_query->the_post();
              $post_id=get_the_ID();
                                         $job_id[]=$post_id;
                                                }
endif; wp_reset_query();?>
<?php if($numb_count-count($job_id)>0):
 $args = array(
'post_type'=>'job_listing',
'post_status'=>'publish',
'posts_per_page' => $numb_count-count($job_id),
'meta_key' => '_featured',                  
'meta_value' => '0',               
'orderby' => 'modified',
'order' => 'DESC');
$post_query = new WP_Query( $args);
      if($post_query->have_posts()):
            while ( $post_query->have_posts() ) { ?>
                        <?php
                        $post_query->the_post();
                            $post_id=get_the_ID();
                                         $job_id[]=$post_id;
                                                }
endif; wp_reset_query(); endif; ?>
  <?php $cout=1; foreach ( $job_id as $jobs) : ?>
  <div class="jobs_line <?= (get_post_meta( $jobs, '_featured', true )==1) ? 'featured' : ''?>" data_id="<?=$jobs?>" style="display:<?= ($cout>$numb) ? 'none' : 'grid'?>" > 
  <div class="wrap_company_logo"><div class="company_logo" style="background-image:url('<?= wp_get_attachment_image_src( get_post_thumbnail_id( $jobs ), 'full' )[0]; ?>');"></div></div>
  <div class="jobs_info">
       <div class="jobs_name"><?= get_the_title( $jobs ) ?></div>
       <div class="jobs_company_name"><?= get_post(get_post_meta( $jobs, '_company_name', true ))->post_title; ?></div>
       <div class="jobs_location"><?= (get_post_meta( $jobs, '_job_location', true )!="") ? get_post_meta( $jobs, '_job_location', true ) : 'Anywhere'; ?></div>
  </div>
  <div class="jobs_time"><?= ash_relative_time($jobs); ?></div>
  </div>
  <?php $cout++;endforeach; ?>
    </div>
    <div class="wrap_ajax_job_list tablet">
  <?php $cout=1; foreach ( $job_id as $jobs) : ?>
<a class="jobs_line <?= (get_post_meta( $jobs, '_featured', true )==1) ? 'featured' : ''?>" data_id="<?=$jobs?>" href="<?= get_permalink($jobs)  ?>" style="display:<?= ($cout>$numb) ? 'none' : 'grid'?>">
  <div class="wrap_company_logo"><div class="company_logo" style="background-image:url('<?= wp_get_attachment_image_src( get_post_thumbnail_id( $jobs ), 'full' )[0]; ?>');"></div></div>
  <div class="jobs_info">
       <div class="jobs_name"><?= get_the_title( $jobs ) ?></div>
       <div class="jobs_company_name"><?= get_post(get_post_meta( $jobs, '_company_name', true ))->post_title ?></div>
       <div class="jobs_location"><?= (get_post_meta( $jobs, '_job_location', true )!="") ? get_post_meta( $jobs, '_job_location', true ) : 'Anywhere'; ?></div>
  </div>
  <div class="jobs_time"><?= ash_relative_time($jobs); ?></div>
 </a>
  <?php $cout++;endforeach; ?>
    </div>
    <div class="wrap_ajax_job_info desktop">
    <?php     $cout=1; foreach ( $job_id as $jobs) : 
    $excerpt = get_the_excerpt($jobs);
    $excerpt = substr($excerpt, 0, 9999);
    $result = substr($excerpt, 0, strrpos($excerpt, ' ')).'...';
    foreach ( get_the_terms( $jobs, 'job_listing_type' ) as $tax ) {  $jobs_type=$tax->name ; }
   ?>
  <div class="show_jobs" style="display:<?= ($cout==1) ? 'block' : 'none'?>" data_id="<?=$jobs?>">
  <div class="wrap_company_logo_show"><div class="company_logo_show" style="background-image:url('<?= wp_get_attachment_image_src( get_post_thumbnail_id( $jobs ), 'full' )[0]; ?>');"></div></div>
  <div class="wrap-content-show">
  <div class="jobs_name"><?= get_the_title( $jobs ) ?></div>
  <div class="jobs_company_name"><?= get_post(get_post_meta( $jobs, '_company_name', true ))->post_title ?></div>
  <div class="jobs_type"><?= $jobs_type ?></div>
  <div class="jobs_location"><?= (get_post_meta( $jobs, '_job_location', true )!="") ? get_post_meta( $jobs, '_job_location', true ) : 'Anywhere'; ?></div>
  <div class="jobs_description"><?= $result; ?></div>
  <div class="wrap-button"><button class="read_more_job" onclick="window.location.href='<?= get_permalink($jobs) ?>'">Read More</button></div>
  </div>
  </div>
   <?php $cout++; endforeach; ?>
  </div>
    <div class="wrap-button"><button class="loadmore_job_list">Load More Listings</button></div>
    </div>
  <script type="text/javascript">jQuery(function($){
  $('.wrap_ajax_job_list.desktop .jobs_line').click(function(){
    var data_id=$(this).attr('data_id');
        $('.show_jobs').each(function(){if($(this).attr('data_id')==data_id){$(this).css('display','block');}else{$(this).css('display','none')}});
    });
    $('.loadmore_job_list').click(function(){
        $('.wrap_ajax_job_list').css("overflow-y","scroll");
        $('.wrap_ajax_job_list').css("height",$('.wrap_ajax_job_list').height());
        $('.wrap_ajax_job_list').css("border-bottom","1px solid #DCDCDC");
        $(this).text("Browse All Jobs");
        $(this).attr("onclick","window.location.href='<?= get_home_url(); ?>/jobs/'");
        if($(window).width()>1200){$('.wrap_ajax_job_list.desktop .jobs_line').each(function(){$(this).css('display','grid')});}else{$('.wrap_ajax_job_list.tablet .jobs_line').each(function(){$(this).css('display','grid')});}
    });
    })
</script>
  <?php
}
add_shortcode( 'home_job_list', 'home_job_list' );
}

/*js bộ lọc mobile*/
if ( ! function_exists( 'js_archive' ) ) {
function js_archive($args,$content) { 
?> 
<div class="add_loc_mobile" style="display:none"></div>
<script type="text/javascript">jQuery(function($){
  $(document).ready(function(){
         $('.close').click(function(){ $('button.btn.sidebar-toggler').click();});
         if($(window).width()<768)
         { 
         $('.add_loc_mobile').after('<a class="huy">Hủy</a><a class="loc">Lọc</a><a class="close">×</a>');
         $('.add_loc_mobile').remove();
          $('li.jobhunt-wpjm-widget-layered-nav-list__item a').removeAttr('href');
         
          $('li.jobhunt-wpjm-widget-layered-nav-list__item').click(function(){
            if(!$(this).hasClass('chosen')){$(this).addClass('chosen')}
            else if($(this).hasClass('chosen')){$(this).removeClass('chosen')}
          });
          $('.huy').click(function(){ $('li.jobhunt-wpjm-widget-layered-nav-list__item').removeClass('chosen');});
          $('.loc').click(function(){
          var location_filter="";
          var category_filter="";
          var level_filter="";
          var work_type_filter="";  
          var url_new="<?=home_url()?>/jobs/?";
          var url_filter=""; 
          var url_array = [];
          var classList;
          $('ul.tax-job_location_city > li.chosen').each(function() { 
            classList = $(this).attr('class').split(/\s+/);
            $.each(classList, function(index, item) {
               if(item!="jobhunt-wpjm-widget-layered-nav-list__item" && item!="jh-wpjm-layered-nav-term" && item!="chosen" && item!="jobhunt-wpjm-widget-layered-nav-list__item--chosen")
               {
                 location_filter+=item+',';
               }
            }); 
          });
          $('ul.tax-job_listing_category > li.chosen').each(function() { 
            classList = $(this).attr('class').split(/\s+/);
            $.each(classList, function(index, item) {
               if(item!="jobhunt-wpjm-widget-layered-nav-list__item" && item!="jh-wpjm-layered-nav-term" && item!="chosen" && item!="jobhunt-wpjm-widget-layered-nav-list__item--chosen")
               {
                 category_filter+=item+',';
               }
            });
             });
          $('ul.tax-job_listing_career_level > li.chosen').each(function() {
            classList = $(this).attr('class').split(/\s+/);
            $.each(classList, function(index, item) {
               if(item!="jobhunt-wpjm-widget-layered-nav-list__item" && item!="jh-wpjm-layered-nav-term" && item!="chosen" && item!="jobhunt-wpjm-widget-layered-nav-list__item--chosen")
               {
                 level_filter+=item+',';
               }
            }); 
         });
          $('ul.tax-job_listing_type > li.chosen').each(function() { 
            classList = $(this).attr('class').split(/\s+/);
            $.each(classList, function(index, item) {
               if(item!="jobhunt-wpjm-widget-layered-nav-list__item" && item!="jh-wpjm-layered-nav-term" && item!="chosen" && item!="jobhunt-wpjm-widget-layered-nav-list__item--chosen")
               {
                 work_type_filter+=item+',';
               }
            }); 
          });
          location_filter=location_filter.slice (0, -1);
          category_filter=category_filter.slice (0, -1);
          level_filter=level_filter.slice (0, -1);
          work_type_filter=work_type_filter.slice (0, -1);
          if (location_filter!="") { url_array[0]="filter_job_location_city="+location_filter+"&query_type_job_location_city=or";}
          if (category_filter!="") { url_array[1]="filter_job_listing_category="+category_filter+"&query_type_job_listing_category=or";}
          if (level_filter!="") { url_array[2]="filter_job_listing_career_level="+level_filter+"&query_type_job_listing_career_level=or";}
          if (work_type_filter!="") { url_array[3]="filter_job_listing_type="+work_type_filter+"&query_type_job_listing_type=or";}
          var url_filter=url_array.join('&');
          window.location.href = url_new+url_filter;
          });
         }
      var box = $(".widget_jobhunt_wpjm_layered_nav.closed");
  function box_check(){ 
 $('.widget_jobhunt_wpjm_layered_nav.closed').each(function() {$(this).removeClass('closed');});
  }
if(!box)
{
    setTimeout( box_check , 100 );
} 
else
{
   setTimeout( box_check , 0 );
}    
    }) })
</script>
  <?php
}
add_shortcode( 'js_archive', 'js_archive' );
}
/*show job ajax by click - home job list*/
function get_slug() {
  ob_start();
  $name_li = (isset($_POST['name_li']))?esc_attr($_POST['name_li']) : '';
  $taxonomy = (isset($_POST['taxonomy']))?esc_attr($_POST['taxonomy']) : '';
  $slug=get_term_by('name', $name_li, $taxonomy);
  echo $slug->slug;
    $result = ob_get_clean();
    wp_send_json_success($result); 
    die();
}
add_action( 'wp_ajax_get_slug', 'get_slug' );
add_action( 'wp_ajax_nopriv_get_slug', 'get_slug' );
if ( ! function_exists( 'home_jobs_show' ) ) {
function home_jobs_show() {
    ob_start();
    $post_query=null;
    $jobs =  (isset($_POST['job_id']))?esc_attr($_POST['job_id']) : '';
    $excerpt = get_the_excerpt($jobs);
    $excerpt = substr($excerpt, 0, 9999);
    $result = substr($excerpt, 0, strrpos($excerpt, ' ')).'...';
    foreach ( get_the_terms( $jobs, 'job_listing_type' ) as $tax ) {  $jobs_type=$tax->name ; } 
    ?>
  <div class="wrap_company_logo_show"><div class="company_logo_show" style="background-image:url('<?= wp_get_attachment_image_src( get_post_thumbnail_id( $jobs ), 'full' )[0]; ?>');"></div></div>
  <div class="wrap-content-show">
  <div class="jobs_name"><?= get_the_title( $jobs ) ?></div>
  <div class="jobs_company_name"><?= get_post(get_post_meta( $jobs, '_company_name', true ))->post_title ?></div>
  <div class="jobs_type"><?= $jobs_type ?></div>
  <div class="jobs_location"><?= get_post_meta( $jobs, '_job_location', true ); ?></div>
  <div class="jobs_description"><?= $result; ?></div>
  <div class="wrap-button"><button class="read_more_job" onclick="window.location.href='<?= get_permalink($jobs) ?>'">Read More</button></div>
  </div>
  <?php
    $result = ob_get_clean();
    wp_send_json_success($result); 
    die();
}
add_action( 'wp_ajax_home_jobs_show', 'home_jobs_show' );
add_action( 'wp_ajax_nopriv_home_jobs_show', 'home_jobs_show' );
}

/*xóa licenses admin setting*/
function remove_licenses_page() {
    $page = remove_submenu_page( 'index.php', 'smyles-licenses' );
}
add_action( 'admin_menu', 'remove_licenses_page', 999 );

/*auto chấp thuận subscriber*/
function change_role()
{
$blogusers = get_users( array( 'role' => array( 'subscriber' ) ) );
foreach ( $blogusers as $user ) {
     if(class_exists('pw_new_user_approve') && $user->pw_user_status=="pending" && $user->erf_active==1){
   pw_new_user_approve()->update_user_status( $user->ID , 'approve' );
}
}
}
add_action( 'admin_init', 'change_role' );

/*auto thêm company vào user*/
function auto_add_company()
{
$blogusers = get_users( array( 'role' => array( 'employer' ) ) );
foreach ( $blogusers as $user ) 
{
  if($user->pw_user_status=="approved" && $user->erf_active==1)
  {
    $company_meta=get_user_meta($user->ID);
    if($company_meta['id_company'][0]=="")
    {
      if(get_page_by_title( $company_meta['company_name'][0], '', 'company' )->post_title=="")
      {
      $post = array(
        'post_title'    => $company_meta['company_name'][0],
        'post_content'  => '',
        'post_author' => $user->ID,
        'meta_input'   => array(
          '_company_phone' => $company_meta['phone_number'][0],
          '_company_location' => $company_meta['district_name'][0],
          '_company_email' => $user->user_email,),
        'post_status'   => 'publish',
        'post_type' => 'company',
      );
      $vnkings_post_id = wp_insert_post($post);
      $posts = new WP_Query(array('post_type' => 'company','post_status'=> 'publish','author' => $user->ID,'posts_per_page'=>1,'orderby' => 'date','order' => 'DESC',));
      $post_s=$posts->post;
      $location=get_term_by('name', $company_meta['city_name'][0] , 'company_location_city')->term_id;
      wp_set_post_terms( $post_s->ID, $location , 'company_location_city' );
      update_user_meta($user->ID,'id_company',$post_s->ID);
      wp_reset_query();
      }
    }
    elseif($company_meta['id_company'][0]!="")
   {
         $company_id=$company_meta['id_company'][0];
         if(get_post_meta( $company_id, '_company_phone', true )!="" && get_post_meta( $company_id, '_company_phone', true )!=$company_meta['phone_number'][0])
          {
             update_user_meta($user->ID,'phone_number',get_post_meta( $company_id, '_company_phone', true ));
          }
         //
         if(get_post_meta( $company_id, '_company_location', true )!="" && get_post_meta( $company_id, '_company_location', true )!=$company_meta['district_name'][0])
         {
              update_user_meta($user->ID,'district_name',get_post_meta( $company_id, '_company_location', true ));
         }
         //
         if(wp_get_post_terms( $company_id, 'company_location_city' , array( 'fields' => 'names' ) )[0]!="")
         {
          $terms_name=wp_get_post_terms( $company_id, 'company_location_city' , array( 'fields' => 'names' ) );
           $val_lo_input="";
           foreach ($terms_name as $value => $val): $val_lo_input.=$val.', '; endforeach;
           $val_lo_input=substr($val_lo_input, 0, -2);
           if($val_lo_input!=$company_meta['city_name'][0])
           {
              update_user_meta($user->ID,'city_name',$val_lo_input);
           }
         }
         //
         if(get_post_meta( $company_id, '_company_email', true )!="" && get_post_meta( $company_id, '_company_email', true )!=$user->user_email)
         {
          wp_update_user( array( 'ID' => $user->ID, 'user_email' => get_post_meta( $company_id, '_company_email', true ) ) );
         }
         //
         if(get_the_title($company_id)!=$company_meta['company_name'][0])
         {
            update_user_meta($user->ID,'company_name',get_the_title($company_id));
         }
         //
         if(get_post_thumbnail_id($company_id)=="")
         {
            $url_custom_thumbnail_id=pn_get_attachment_id_from_url(get_home_url()."/wp-content/uploads/2018/07/company-custom-logo.png");
            set_post_thumbnail( $company_id, $url_custom_thumbnail_id );
         }
      
   }
  }
}
foreach ( $blogusers as $user ) 
{
  $company_meta_2=get_user_meta($user->ID);
  foreach (get_posts(array('post_type' => 'company','post_status'=>'any','nopaging' => true)) as $value => $company)
  {      
    if(get_post_thumbnail_id($company->ID)=="")
      {
        $url_custom_thumbnail_id=pn_get_attachment_id_from_url(get_home_url()."/wp-content/uploads/2018/07/company-custom-logo.png");
        set_post_thumbnail( $company->ID, $url_custom_thumbnail_id );
      }
    if($company->ID==$company_meta_2['id_company'][0] && $company->post_author!=$user->ID)
    {
      $my_post = array(
        'ID'           => $company->ID,
        'post_author'  => $user->ID,
      );
      wp_update_post( $my_post );
    }
  }
}
}
add_action( 'admin_init', 'auto_add_company' );

/*lưu session trang gần nhất*/
function last_page_head()
{ 
$url_current=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if(strpos($url_current, 'login-for-recruiter')==false && strpos($url_current,'login-for-job-seeker')==false  && strpos($url_current,'wp-login.php')==false )
{
$_SESSION['url_last_page_head']=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}
else if(strpos($url_current, 'login-for-recruiter' )>-1){ $_SESSION['id_last_page']=3317; }
else if(strpos($url_current,'login-for-job-seeker')>-1){ $_SESSION['id_last_page']=239; }
 }
add_action('wp_head','last_page_head');

/*trang đăng job*/
function submit_job_form_code()
{
if( !is_user_logged_in() ) { ?>
      <script type="text/javascript">
        jQuery(function($){
            window.location.href="<?= get_home_url(); ?>/login-for-recruiter/";
        })
      </script>
  <?php 
 } 
 elseif( is_user_logged_in() ) 
 {
    $user = wp_get_current_user();
       $user_roles=$user->roles;
       if($user_roles[0]=="subscriber")
       { 
        ?>
         <div class="field account-sign-in">
        You must be an employer to create a new listing.
         </div>
      <?php
       }
       else
       { 
         echo do_shortcode('[submit_job_form_new]');
       }
 }
}
add_shortcode( 'submit_job_form_code', 'submit_job_form_code' );

function show_user()
{
  if(is_user_logged_in())
  {
  $current_user = wp_get_current_user();
    $user_name =  $current_user->user_login;
    ?>
    <script type="text/javascript">
    jQuery(function($){
      $('.header-menu.yamm li:first-child').before('<li class="wrap-user"><div class="text-hi">hi</div><div class="text-user-name">, <?=$user_name?></div>&nbsp;-&nbsp;<a class="log-out-header" href="<?php echo wp_logout_url( $_SESSION['url_last_page_head'] ) ?>" >Log Out</a></li>');
    })
    </script>
  <?php
    }
}
add_action( 'wp_head', 'show_user' );

function submit_job_form_new()
{
$user_id = get_current_user_id();
$current_user = wp_get_current_user();
$vnkings =  $current_user->roles;
if($vnkings == "administrator") { $vnstatus = "publish"; } else { $vnstatus = "pending"; } 
?>
       <form id="new_post" class="wrap-form login info space-between post-job " method="post" action="" >
        <h1 class="title-form-contain w100">Create Job</h1>
        <div class="form-group-job-title w100">
            <label for="job_title">Job Title</label>
            <input type="text" name="job_title" class="form-control" placeholder="Tiêu đề">
        </div>
        <div class="form-group-job-description w100">
          <label for="job_content">Job Description </label>
          <?php wp_editor( '', 'job-description', array( 'textarea_name' => 'job_content' ));?>
        </div>
        <div class="form-group-job-requirement w100">
          <label for="job_requirement">Job Requirement </label>
          <?php wp_editor( '', 'job_requirement', array( 'textarea_name' => 'job_requirement' ));?>
        </div>
        <div class="form-group-job-type w50">
          <label for="job_listing_type">Job Type</label>
         <?php drop_taxonomy('job_listing_type','-- select job type --'); ?>       
        </div>
        <div class="form-group-job-category w50 more-option">
          <label for="job_listing_category">Category</label>
          <ul class="ul_job_listing_category">
        <li class="li_job_listing_category_last"><input class="li-input-text-show_job_listing_category" type="text" data-for="job_listing_category"/></li>
        </ul>
          <input type="hidden" name="job_listing_category_input" class="job-listing-category" />
          <div class="job_listing_category_autocomplete" style="display: none;">
            <?php $terms=get_terms(['taxonomy' => 'job_listing_category','hide_empty' => false,]); 
            foreach ( $terms as $terms_name) : ?>
              <div class="autocomplete_text" style="display: none"><?=$terms_name->name?></div>
            <?php endforeach;?>
              <div class="autocomplete_text not_found" style="display: none">No Category found</div> 
          </div>  
        </div>
        <div class="form-group-job-career-level w50">
          <label for="job_listing_career_level">Level</label>
         <?php drop_taxonomy('job_listing_career_level','-- select job career level --'); ?>       
        </div>
        <div class="form-group-job-industry w50 more-option">
          <label for="job_listing_industry">Industry</label>
          <ul class="ul_job_listing_industry">
        <li class="li_job_listing_industry_last"><input class="li-input-text-show_job_listing_industry" type="text" data-for="job_listing_industry"/></li>
        </ul>
          <input type="hidden" name="job_listing_industry_input" class="job-listing-industry" />
          <div class="job_listing_industry_autocomplete" style="display: none;">
            <?php $terms=get_terms(['taxonomy' => 'job_listing_industry','hide_empty' => false,]); 
            foreach ( $terms as $terms_name) : ?>
              <div class="autocomplete_text" style="display: none"><?=$terms_name->name?></div>
            <?php endforeach;?>
              <div class="autocomplete_text not_found" style="display: none">No Industry found</div> 
          </div>     
        </div>
        <div class="form-group-job-experience w50">
          <label for="job_listing_experience">Experience</label>
         <?php drop_taxonomy('job_listing_experience','-- select job experience --'); ?>       
        </div>
        <div class="form-group-job-qualification w50">
          <label for="job_listing_qualification">Qualification</label>
         <?php drop_taxonomy('job_listing_qualification','-- select job qualification --'); ?>       
        </div>
        <div class="form-group-job-location-city w100 more-option">
          <label for="job_location_city">Work Location</label>
        <ul class="ul_job_location_city">
        <li class="li_job_location_city_last"><input class="li-input-text-show_job_location_city" type="text" data-for="job_location_city"/></li>
        </ul>
          <input type="hidden" name="job_location_city_input" class="job-location-city" />
          <div class="job_location_city_autocomplete" style="display: none;">
            <?php $terms=get_terms(['taxonomy' => 'job_location_city','hide_empty' => false,]); 
            foreach ( $terms as $terms_name) : ?>
              <div class="autocomplete_text" style="display: none"><?=$terms_name->name?></div>
            <?php endforeach;?>
              <div class="autocomplete_text not_found" style="display: none">No Work Location found</div>
          </div> 
        </div>
        <div class="form-group-job-skill-requirement w100">
          <label for="skill_requirement">Skill requirement</label>
          <input type="text" name="skill_requirement" class="job-skill-requirement" />
        </div>
       <div class="form-group-job-expires-datepicker w100">
          <label for="_job_expires-datepicker">Application deadline</label>
         <select class="search_category" name="_job_expires-datepicker" style="color:#555">
            <option value="7d" style="color:#555" selected>Expired in 7 days</option>
            <option value="15d" style="color:#555">Expired in 15 days</option>
            <option value="30d" style="color:#555">Expired in 30 days</option>
         </select>      
        </div>
        <input type="hidden" name="add_new_post" value="post" />
        <?php wp_nonce_field( 'post_nonce', 'post_nonce_field' ); ?>
        <div class="form-group">
            <div class="col-sm-12" style="padding-left:0;">
              <button type="submit" class="btn btn-primary">Post Job</button>
            </div>
        </div>
    </form>
    <script type="text/javascript">jQuery(function($){
      function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
}
      function out_click(name)
        {
          $(document).on('click', function (e) 
          {
            if ($(e.target).closest('.'+name+'_autocomplete').length === 0) 
            {
              $('.'+name+'_autocomplete').hide();
            }
          });
        }
        out_click('job_location_city');
        out_click('job_listing_industry');
        out_click('job_listing_category');
      function focus(name)
        {
          $('ul.ul_'+name).click(function(){ $('input.li-input-text-show_'+name).focus(); });
        }
        focus('job_location_city');
        focus('job_listing_industry');
        focus('job_listing_category');
      function search(name)
        {  
          $('input.li-input-text-show_'+name).on("change paste keyup", function() 
          {
            if($(this).val()!="")
              {
                var text=xoa_dau($(this).val());
                count_found=0;
                text_input_show=new RegExp('^'+text+'.*', 'i');
                $('.'+name+'_autocomplete .autocomplete_text').each(function()
                  {  
                    var text_option=$(this).text();
                    if( text_input_show.test(text_option) )
                      { 
                        $(this).css('display','block');
                        $('.'+name+'_autocomplete').css('display','block');
                        count_found++;
                      }
                    else 
                      {
                        $(this).css('display','none');
                      }
                  });
                if(count_found==0)
                  { 
                    $('.'+name+'_autocomplete .autocomplete_text.not_found').css('display','block'); 
                    $('.'+name+'_autocomplete').css('display','block');
                  }
              }
            else
              {
                $('.'+name+'_autocomplete').css('display','none');
              }
          });
        search_click(name);
        }
        search('job_location_city');
        search('job_listing_industry');
        search('job_listing_category');
        function search_click(name)
        {
          $('div.more-option .'+name+'_autocomplete .autocomplete_text').click(function()
            {
              var input_value=$('input[name="'+name+'_input"]').val();
              if(input_value.indexOf($(this).text())== -1 )
                {
                  $('input[name="'+name+'_input"]').val(input_value+=$(this).text()+', ');
                  $('.li_'+name+'_last').before('<li class="li_'+name+'" title="'+$(this).text()+'"><p class="text-val">'+$(this).text()+'</p><p class="li-value-delete">×</p></li>');
                  $(this).css('display','none');
                  xoa_li(name);
                }
              else if(input_value.indexOf($(this).text())> -1 )
                {   
                  var val_dele=$(this).text()+', ';
                  var new_val=input_value.replaceAll(val_dele,"");
                  $('input[name="'+name+'_input"]').val(new_val);
                  $('li.li_'+name+'').each(function() {if( val_dele.indexOf($(this).text().replace("×",""))>-1) $(this).remove()});
                  $(this).css('display','none');
                }
                  $('.'+name+'_autocomplete').css('display','none');
                  $(this).parents('div.more-option').find('.li-input-text-show_'+name+'').val('');
                  $(this).parents('div.more-option').find('.li-input-text-show_'+name+'').focus();
          })
        }
        function xoa_li(name)
        {
            $('p.li-value-delete').click(function(e)
            { 
              var text_del=$(this).parents('li').text().replace("×","")+', '; 
              var new_val=$('input[name="'+name+'_input"]').val().replaceAll(text_del,"");
              $('input[name="'+name+'_input"]').val(new_val);
              $(this).parents('li').remove();
            });
        }        
})
    </script>
<?php 
if( $_SERVER['REQUEST_METHOD'] == 'POST' && !empty( $_POST['add_new_post'] ) && isset( $_POST['post_nonce_field'] ) && wp_verify_nonce( $_POST['post_nonce_field'], 'post_nonce' )) {

if (isset($_POST['job_title'])) {
    $job_title = $_POST['job_title'];
}
if (isset($_POST['job_content'])) {
    $job_content = $_POST['job_content'];
}
if (isset($_POST['job_requirement'])) {
    $job_requirement = $_POST['job_requirement'];
}
if (isset($_POST['job_listing_type'])) {
    $job_listing_type = $_POST['job_listing_type'];
}
if (isset($_POST['job_listing_category_input'])) {
    $job_listing_category[] = explode(", ",$_POST['job_listing_category_input']);
}
if (isset($_POST['job_listing_career_level'])) {
    $job_listing_career_level = $_POST['job_listing_career_level'];
}
if (isset($_POST['job_listing_industry_input'])) {
    $job_listing_industry[] = explode(", ",$_POST['job_listing_industry_input']);
}
if (isset($_POST['job_listing_experience'])) {
    $job_listing_experience = $_POST['job_listing_experience'];
}
if (isset($_POST['job_listing_qualification'])) {
    $job_listing_qualification = $_POST['job_listing_qualification'];
}
if (isset($_POST['job_location_city_input'])) {
    $job_location_city[] = explode(", ",$_POST['job_location_city_input']);
}
if (isset($_POST['skill_requirement'])) {
    $skill_requirement = $_POST['skill_requirement'];
}
if (isset($_POST['_job_expires-datepicker'])) {
  if($_POST['_job_expires-datepicker']=="7d")
  {
    $_job_expires = time()+ 604800;
  }
  elseif($_POST['_job_expires-datepicker']=="15d")
  {
    $_job_expires = time()+ 1296000;
  }
  else
  {
    $_job_expires = time()+ 2592000;
  }
}
if($_POST['job_title']!="")
{
$post = array(
    'post_title'    => wp_strip_all_tags($job_title),
    'post_content'  => $job_content,
    'meta_input'   => array(
    'job_requirement' => $job_requirement,
    '_job_expires' => date("Y-m-d", $_job_expires),
    'skill_requirement' => $skill_requirement,
    ),
    'post_status'   => $vnstatus,
    'post_type' => 'job_listing',
);
$vnkings_post_id = wp_insert_post($post);
$user = wp_get_current_user();
$posts = new WP_Query(array('post_type' => 'job_listing','post_status'=> array('any','expired'),'author' => $user->ID,'posts_per_page'=>1,'orderby' => 'date','order' => 'DESC',));
$location=array();
foreach ($job_location_city[0] as $location_name) {
$location[]=get_term_by('name', $location_name, 'job_location_city')->term_id;
}

$job_industry=array();
foreach ($job_listing_industry[0] as $job_listing_industry_name) {
$job_industry[]=get_term_by('name', $job_listing_industry_name, 'job_listing_industry')->term_id;
}

$job_category=array();
foreach ($job_listing_category[0] as $job_listing_category_name) {
$job_category[]=get_term_by('name', $job_listing_category_name, 'job_listing_category')->term_id;
}
$post_s=$posts->post;
wp_set_post_terms( $post_s->ID, array(get_term_by('name', $job_listing_type, 'job_listing_type')->term_id), 'job_listing_type' );
wp_set_post_terms( $post_s->ID, array(get_term_by('name', $job_listing_career_level, 'job_listing_career_level')->term_id), 'job_listing_career_level' );
wp_set_post_terms( $post_s->ID, array(get_term_by('name', $job_listing_experience, 'job_listing_experience')->term_id), 'job_listing_experience' );
wp_set_post_terms( $post_s->ID, array(get_term_by('name', $job_listing_qualification, 'job_listing_qualification')->term_id), 'job_listing_qualification' );
wp_set_post_terms( $post_s->ID, $location , 'job_location_city' );
wp_set_post_terms( $post_s->ID, $job_industry, 'job_listing_industry' );
wp_set_post_terms( $post_s->ID, $job_category, 'job_listing_category' );
wp_reset_query();
?>
<script type="text/javascript">
  jQuery(function($){
     $('.wrap-form.login.info').after('<div id="error-form" class="error-form"><div class="warp-error"><div class="button-error-close">×</div><div class="img-error"></div><p class="error-text">Post job thành công, vui lòng chờ quản trị viên phê duyệt</p></div></div>');
     $('.button-error-close').click(function(){ $('#error-form').remove(); window.location.href ="<?= $_SESSION['url_last_page_head'] ?>"; });
     $('#error-form').click(function(){ $(this).remove();window.location.href ="<?= $_SESSION['url_last_page_head'] ?>"; });
  })
</script>
<?php
}
}
}
add_shortcode( 'submit_job_form_new', 'submit_job_form_new' );
function drop_taxonomy($taxonomy,$placeholder)
{ ?>
<select class="search_category" name="<?= $taxonomy ?>">
                            <option value="" style="color:#a7a7a7"><?= $placeholder ?></option>
                           <?php
$category_select=array();
$terms2=get_terms([
    'taxonomy' => $taxonomy,
    'hide_empty' => false,
]); foreach ( $terms2 as $terms_name2 ) : $category_select[]=$terms_name2->name;  endforeach;
?>
                            <?php foreach ( $category_select as $category) : ?>
                            <option value="<?= $category ?>" style="color:#555555"><?= $category ?></option>
                            <?php endforeach; ?>
 </select>
<?php }
function drop_taxonomy2($taxonomy,$placeholder,$id_post)
{ 
$val_post[]=wp_get_post_terms( $id_post, $taxonomy, array( 'fields' => 'names' ) );
  ?>
<select class="search_category" name="<?= $taxonomy ?>">
                            <option value="" style="color:#a7a7a7"><?= $placeholder ?></option>
                           <?php
$category_select=array();
$terms2=get_terms([
    'taxonomy' => $taxonomy,
    'hide_empty' => false,
]); foreach ( $terms2 as $terms_name2 ) : $category_select[]=$terms_name2->name;  endforeach; 
?>
                            <?php foreach ( $category_select as $category) : if($category!="All jobs"): $match=0; foreach ($val_post[0] as $value => $valpost) {
                              if($valpost==$category){$match=1;}
                            } ?>
                            <option value="<?= $category ?>" style="color:#555555" <?= ($match==1) ? 'selected' : '' ?> ><?= $category ?></option>
                            <?php endif; endforeach; ?>
 </select>
<?php }

/*chuyển hướng đăng nhập*/
function my_login_redirect( $redirect_to, $request, $user ) {
  global $user;
  if ( isset( $user->roles ) && is_array( $user->roles ) ) {

    if ( in_array( 'administrator', $user->roles ) ) {
      return admin_url();
    } else {
      if(isset($_SESSION['url_last_page_head']))
      {
        return $_SESSION['url_last_page_head'];
      }
      else
      {
        return home_url('/jobs/');
      }
    }
  } else {
    return $redirect_to;
  }
}
add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

/*chuyển hướng trang login*/
function redirect_login_page() {
$login_page = home_url('wp-login.php');
$page_viewed = basename($_SERVER['REQUEST_URI']);
 
if($page_viewed == "wp-admin")
{
	if(!is_user_logged_in())
	{
        wp_redirect($login_page);
        exit;
    }
}
}
add_action('init','redirect_login_page');
//
function login_failed() {
  if(isset($_SESSION['id_last_page']))
  {
    if($_SESSION['id_last_page']==239)
    {
      $login_page = home_url('/login-for-job-seeker/');
    wp_redirect( $login_page . '?login=failed' );
    exit;
    }
    else if ($_SESSION['id_last_page']==3317)
    {  
      $login_page = home_url('/login-for-recruiter/');
     wp_redirect( $login_page . '?login=failed' );
    exit;
    }
  }
}
add_action( 'wp_login_failed', 'login_failed' );
 function verify_username_password( $user, $username, $password ) {
  if(isset($_SESSION['url_last_page_head']))
  {
    if(strpos($_SESSION['url_last_page_head'], 'book-mark'))
    {
      $login_page = home_url('/login-for-job-seeker/');
          if( $username == "" || $password == "" ) {
        wp_redirect( $login_page . "?login=empty" );
        exit;
       } elseif(get_user_by( 'login', $username )->pw_user_status=="pending" && $login_failed==0)
       {
        wp_redirect( $login_page . "?login=empty&status=pending" );
        exit;
       }
    }
    else if (strpos($_SESSION['url_last_page_head'], 'job-dashboard'))
    {  
      $login_page = home_url('/login-for-recruiter/');
          if( $username == "" || $password == "" ) {
        wp_redirect( $login_page . "?login=empty" );
        exit;
       } elseif(get_user_by( 'login', $username )->pw_user_status=="pending" &&  $login_failed==0)
       {
        wp_redirect( $login_page . "?login=empty&status=pending" );
        exit;
       }
    }
  }
}
add_filter( 'authenticate', 'verify_username_password', 1, 3);

/*trang tìm lại pass*/
function header_lost_pass() {
if($_GET['action']=='lostpassword' || $_GET['checkemail']=='confirm' || $_GET['action']=='rp')
{
get_header(); 
}
if($_GET['checkemail']=='confirm')
{ ?>
<script type="text/javascript">
	jQuery(function($){
		$('p.message.register').text('Passcode reset link has been sent to your email. Please check.');
	})
</script>
<?php 
}   
}
add_filter( 'login_header', 'header_lost_pass',1);
//
function footer_lost_pass() {
if($_GET['action']=='lostpassword' || $_GET['checkemail']=='confirm' || $_GET['action']=='rp'){
get_footer();
}
}
add_action( 'login_footer', 'footer_lost_pass',999);

/*background footer*/
function footerbg(){ ?>
<script type="text/javascript">
        jQuery(function($){
            $(':root').css("--background-image","url('<?=get_post_meta( 217, 'footer_background', true )['guid']?>')");
        })
    </script>
<?php }
add_action('wp_head','footerbg');
    
/*autocomplate search*/
if ( ! function_exists( 'search_autocomplete_text' ) ) {
function search_autocomplete_text() {
    ob_start();
    $text =  (isset($_POST['text']))?esc_attr($_POST['text']) : '';
    //$autocomplete=array();
    //$autocomplete_2=array();

    function title_filter( $where, &$wp_query )
    {
      global $wpdb;
      // 2. pull the custom query in here:
      if ( $search_term = $wp_query->get( 'search_prod_title' ) ) 
      {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( like_escape( $search_term ) ) . '%\'';
      }
      return $where;
    }
    $args = array(
    'post_type' => 'job_listing',
    'posts_per_page' => 3,
    'paged' => 1,
    'search_prod_title' => $text,
    'post_status' => 'publish',
    'orderby'     => 'title', 
    'order'       => 'ASC'
    );
    add_filter( 'posts_where', 'title_filter', 10, 2 );
    $wp_query = new WP_Query($args);
    remove_filter( 'posts_where', 'title_filter', 10 );
    if($wp_query->have_posts())
    {
      echo '<div class="title_autocomplete_text">Title</div>';
    }
    while ( $wp_query->have_posts() ) 
    {  
      $wp_query->the_post(); 
      echo '<div class="autocomplete_text">'.get_the_title().'</div>';
    }
    wp_reset_query();
    $args_2 = array(
    'post_type' => 'company',
    'posts_per_page' => 3,
    'paged' => 1,
    'search_prod_title' => $text,
    'post_status' => 'publish',
    'orderby'     => 'title', 
    'order'       => 'ASC'
    );
    add_filter( 'posts_where', 'title_filter', 10, 2 );
    $wp_query_2 = new WP_Query($args_2);
    remove_filter( 'posts_where', 'title_filter', 10 );
    if($wp_query_2->have_posts())
    {
      echo '<div class="title_autocomplete_text">Company</div>';
    }
    while ( $wp_query_2->have_posts() ) 
    {  
      $wp_query_2->the_post(); 
      echo '<div class="autocomplete_text">'.get_the_title().'</div>';
    }
    wp_reset_query();
    if(!$wp_query->have_posts() && !$wp_query_2->have_posts())
    {
      echo '<div class="title_autocomplete_text no_text">No text found</div>';
    }
    $result = ob_get_clean();
    wp_send_json_success($result); 
    die();
}
add_action( 'wp_ajax_search_autocomplete_text', 'search_autocomplete_text' );
add_action( 'wp_ajax_nopriv_search_autocomplete_text', 'search_autocomplete_text' );
}

/*sidebar theo phân quyền*/
function sidebar_user()
{  
  if( is_user_logged_in() ) 
 { ?>
   <script type="text/javascript">
      jQuery(function($){
             $('.kc_column:first-child').css('display','');
             $('.kc_column:last-child').css('width','');
      })
    </script>
  <?php 
    $user = wp_get_current_user();
       $user_roles=$user->roles;
       if($user_roles[0]!="subscriber")
       {    $url_current= home_url( '/job-dashboard/' ); ?>
            <a href="<?= $url_current.'?pages=company-profile'?>" class="wrap-company-profile <?=($_GET['pages']=='company-profile') ? 'chosen' : '' ?>" title="company profile"><span class="link-inner">Company Profile</span></a>
            <a href="<?= $url_current ?>" class="wrap-manage-jobs <?=($_GET['pages']=='') ? 'chosen' : '' ?>" title="manage jobs"><span class="link-inner">Manage Jobs</span></a>
            <a href="<?= $url_current.'?pages=post-a-new-job'?>" class="wrap-post-a-new-job <?=($_GET['pages']=='post-a-new-job') ? 'chosen' : '' ?>" title="post a new job"><span class="link-inner">Post a New Job</span></a>
            <a href="<?= $url_current.'?pages=change-password'?>" class="wrap-change-password <?=($_GET['pages']=='change-password') ? 'chosen' : '' ?>" title="changer password"><span class="link-inner">Change Password</span></a>
            <?php $text= 'Log Out';
            $logout_redirect = home_url( '/login-for-recruiter/' ); 
            echo '<a href="'. wp_logout_url( $logout_redirect ) .'" title="'. esc_attr( $text ) .'" class="button-log-out"><span class="link-inner">'. strip_tags( $text ) .'</span></a>';
       }
       else 
       {    $url_current= home_url( '/book-mark/' ); ?>
            <a href="<?= $url_current.'?pages=my-profile'?>" class="wrap-my-profile <?=($_GET['pages']=='my-profile') ? 'chosen' : '' ?>" title="my profile"><span class="link-inner">My Profile</span></a>    
            <a href="<?= $url_current?>" class="wrap-my-joblist <?=($_GET['pages']=='') ? 'chosen' : '' ?>" title="my joblist"><span class="link-inner">My Joblist</span></a>
            <?php $text= 'Log Out';
            $logout_redirect = home_url( '/login-for-job-seeker/' ); 
            echo '<a href="'. wp_logout_url( $logout_redirect ) .'" title="'. esc_attr( $text ) .'" class="button-log-out"><span class="link-inner">'. strip_tags( $text ) .'</span></a>';
       }
 }
 else
 { ?>
    <script type="text/javascript">
      jQuery(function($){
             $('.kc_column:first-child').css('display','none');
             $('.kc_column:last-child').css('width','100%');
      })
    </script>
  <?php }
}
add_shortcode( 'sidebar_user', 'sidebar_user' );

/*trang book mark*/
function my_bookmarks_code()
{
 if( !is_user_logged_in() ) { ?>
      <script type="text/javascript">
        jQuery(function($){
            window.location.href="<?= get_home_url(); ?>/login-for-job-seeker/";
        })
      </script>
  <?php 
} 
 elseif( is_user_logged_in() ) 
 {
  $user = wp_get_current_user();
       $user_roles=$user->roles;
       if($user_roles[0]!="employer")
       { 
        if(isset($_GET['pages']) && $_GET['pages']=='my-profile' ){ echo do_shortcode('[change_password_subscriber]'); }
        else {  echo do_shortcode('[my_bookmarks_new]'); }
       }
       else
       { ?>
         <script type="text/javascript">
        jQuery(function($){
            window.location.href="<?= get_home_url(); ?>/job-dashboard/";
        })
      </script>
      <?php }
 }
}
add_shortcode( 'my_bookmarks_code', 'my_bookmarks_code' );

function get_user_bookmarks_new( $user_id = 0, $limit = 0, $offset = 0, $orderby_key = 'date', $order_dir = 'ASC' ) {
    global $wpdb;

    if ( ! $user_id && is_user_logged_in() ) {
      $user_id = get_current_user_id();
    } elseif ( ! $user_id ) {
      return false;
    }

    $order_options = array(
      'date'       => '`bm`.`date_created`',
      'post_title' => '`p`.`post_title`',
      'post_date'  => '`p`.`post_date`',
    );

    $order_by = $order_options['date'];
    if ( isset( $order_options[ $orderby_key ] ) ) {
      $order_by = $order_options[ $orderby_key ];
    }

    $order_dir = in_array( strtoupper( $order_dir ), array( 'ASC', 'DESC' ), true ) ? strtoupper( $order_dir ) : 'ASC';

    if ( $limit > 0 ) {
      $sql_query = $wpdb->prepare( "SELECT SQL_CALC_FOUND_ROWS `bm`.* FROM `{$wpdb->prefix}job_manager_bookmarks` `bm` " .
              "LEFT JOIN `{$wpdb->posts}` `p` ON `bm`.`post_id`=`p`.`ID` " .
              "WHERE `user_id` = %d  AND `p`.`post_status` = 'publish' " .
              "ORDER BY {$order_by} {$order_dir} ".
              "LIMIT %d, %d;", $user_id, $offset, $limit );
      $results     = $wpdb->get_results( $sql_query );
      $max_results = $wpdb->get_var( "SELECT FOUND_ROWS()" );

      return (object) array(
        'max_found_rows' => $max_results,
        'max_num_pages'  => ceil( $max_results / $limit ),
        'results'        => $results
      );
    } else {
      $sql_query = $wpdb->prepare( "SELECT `bm`.* FROM `{$wpdb->prefix}job_manager_bookmarks` `bm` " .
                     "LEFT JOIN `{$wpdb->posts}` `p` ON `bm`.`post_id`=`p`.`ID` " .
                     "WHERE `user_id` = %d AND `p`.`post_status` = 'publish' " .
                     "ORDER BY {$order_by} {$order_dir}", $user_id );
      return $wpdb->get_results( $sql_query );
    }
  }
function my_bookmarks_new()
{
    $atts = shortcode_atts( array(
      'posts_per_page' => 10,
      'orderby'        => 'date', // Options: date, post_date, post_title
      'order'          => 'DESC',
    ), $atts );
  ob_start();
    wp_enqueue_script( 'wp-job-manager-bookmarks-bookmark-js' );
      $bookmarks = get_user_bookmarks_new( get_current_user_id(), $atts['posts_per_page'], ( max( 1, get_query_var('paged') ) - 1 ) *  $atts['posts_per_page'], $atts['orderby'], $atts['order'] );
      $count=$bookmarks->max_found_rows;
            $book=$bookmarks->results;
       ?>
  <div class="wrap-form login info">
     <h1 class="title-form-contain">My Joblist</h1>
            <?php if(count($book)>0){ ?> 
              <ul class="job_listings list">
                <?php
                 foreach ( $book as $value => $bookmark ) : 
                   $posts_url=get_post_permalink($bookmark->post_id);
                   $posts_url_thumbnail=get_the_post_thumbnail_url($bookmark->post_id,'full');
                   $posts_title=get_the_title($bookmark->post_id);
                   $posts_company= get_post(get_post_meta( $bookmark->post_id, '_company_name', true ))->post_title;
                   $job_expires = get_post_meta( $bookmark->post_id, '_job_expires', true );
                   $company_email= get_post_meta( $bookmark->post_id, '_application', true );
              ?>
        <li class="job_listing" data-longitude="" data-latitude="">
      <a href="<?= $posts_url ?>"><div class="job-listing-company-logo"><img class="company_logo" src="<?= $posts_url_thumbnail ?>"></div></a>
      <div class="job-details ">
      <a href="<?= $posts_url?>">
        <div class="job-details-inner">
                       <h3 class="job-listing-loop-job__title"><?= $posts_title ?></h3>
                         <div class="job-listing-company company"><strong><?= $posts_company ?></strong></div>        
                             <div class="expires"><div class="text">Hạn nộp hồ sơ:&nbsp;</div><div class="time-expires"><?= $job_expires ?></div></div>
            </div>
        </a>
       <div class="job-manager-bookmark-actions">
              <?php
                $actions = apply_filters( 'job_manager_bookmark_actions', array(
                  'delete' => array(
                    'label' => __( 'Delete', 'wp-job-manager-bookmarks' ),
                    'url'   =>  wp_nonce_url( add_query_arg( 'remove_bookmark', $bookmark->post_id ), 'remove_bookmark' )
                  )
                ), $bookmark );
                foreach ( $actions as $action => $value ) {
                  echo '<a href="' . esc_url( $value['url'] ) . '" class="job-manager-bookmark-action-' . $action . '">' . $value['label'] . '</a>';
                }
              ?>
    </div>
        </div>
        </li>
    <?php endforeach; ?> </ul> <?php } 
  else
      { ?>                 <div class="job_listings list null">
                           <a href="<?= get_home_url(); ?>/jobs/"><div class="img-bookmark-null"></div></a>
                           <div class="header-text">Tap the heart to save jobs</div><div class="content-text"><h3>You haven't saved any jobs yet. Search for jobs and tap on the heart to save them here</h3></div>
                           <a class="button-search" href="<?= get_home_url(); ?>/jobs/"><i class="la la-search"></i>Search jobs</a>
                           </div>
    <?php } ?>
    <div class="warp-paging">
  <?php  get_job_manager_template( 'pagination.php', array( 'max_num_pages' => CEIL($count/$atts['posts_per_page']) ) ); ?>
   </div>
   </div> 
   <script type="text/javascript">
    jQuery(function($){
      var old_book_mark_count="<?= $count ?>";
      var posts_per_page="<?= $atts['posts_per_page']?>";
      $('.job-manager-bookmark-action-delete').click(function(){
        var $div = $(this);
var observer = new MutationObserver(function(mutations) {
  mutations.forEach(function(mutation) {
    if (mutation.attributeName === "class") {
      $.ajax({
                    type : "post", 
                    dataType : "json", 
                    url : '<?php echo admin_url('admin-ajax.php');?>', 
                    data : {
                        action: "check_bookmark_ajax", 
                        old_book_mark_count: old_book_mark_count,
                        posts_per_page: posts_per_page
                    },
                    context: this,
                    beforeSend: function(){
                    },
                    success: function(response) {
                        if(response.success)
                    {
                         if(response.data.indexOf('changer')> -1)
                        {
                            location.reload();
                        }
                    }
                    else {
                            alert('Đã có lỗi xảy ra');
                        }
                    },
                    error: function(){
                    }
                })
    }
  });
});
observer.observe($div[0], {
  attributes: true
});
      })
    })
   </script>
<?php
    return ob_get_clean();
}
add_shortcode( 'my_bookmarks_new', 'my_bookmarks_new' );
function check_bookmark_ajax() {
    ob_start();
    $old_book_mark_count =  (isset($_POST['old_book_mark_count']))?esc_attr($_POST['old_book_mark_count']) : '';
    $posts_per_page =  (isset($_POST['posts_per_page']))?esc_attr($_POST['posts_per_page']) : '';
    $bookmarks = get_user_bookmarks_new( get_current_user_id(), $posts_per_page, ( max( 1, get_query_var('paged') ) - 1 ) *  $posts_per_page );
    $count=$bookmarks->max_found_rows;
    if($old_book_mark_count!=$count)
    {
      echo'changer';
    }
    $result = ob_get_clean();
    wp_send_json_success($result); 
    die();
}
add_action( 'wp_ajax_check_bookmark_ajax', 'check_bookmark_ajax' );
add_action( 'wp_ajax_nopriv_check_bookmark_ajax', 'check_bookmark_ajax' );

/*trang đổi pass subcriber*/
function change_password_subscriber()
{    $user = wp_get_current_user();
     $email=$user->user_email;
     $old_password=$user->user_pass;
     $user_id=$user->ID;
  ?>
  <div class="wrap-form login info">
     <h1 class="title-form-contain">My Profile</h1>
        <p class="first">
            <label class="label-email">Email</label>
            <input type="text" name="email" id="pass1" class="input email" value="<?= $email ?>" autocomplete="off" />
        </p>
        <p>
            <label class="old-password">Password</label>
            <input type="password" name="password"  class="input password" value="<?= $old_password ?>" autocomplete="off" />
        </p>
        <p class="new none">
            <label class="new-password">New Password</label>
            <input type="password" name="new-password" class="input new-password"  value="" autocomplete="off" placeholder="Nhập new password" />
        </p>
        <p class="confirm none">
            <label class="confirm-password">Confirm Password</label>
            <input type="password" name="confirm-password" class="input confirm-password"   value="" autocomplete="off" placeholder="Confirm password"/>
        </p>
        <p class="wrap-button">
           <a class="edit">EDIT</a><a class="save none">SAVE</a><a class="cancel none">CANCEL</a>
        </p>
    </div>
    <script type="text/javascript">
      function close_error() {
  var element = document.getElementById("error-form");
  element.classList.add("none");}
      jQuery(function($){
        $('.edit').click(function(){
          $('.password').val(''); $(this).addClass('none'); $('.save').removeClass('none');$('.cancel').removeClass('none'); $('.confirm').removeClass('none'); $('.new').removeClass('none');$('input.email').val('');  
          $(document).keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
       $('.save').click();   
    }
});
        });
        $('.cancel').click(function(){
          location.reload();
        });
        $("input.email").on("focusout", function() {
   var input_mail=$(this);
  var re1_mail = /^[a-zA-Z0-9@.!#$%&'*+/\\=?^_`{|}~-]*$/;
  var text_valid_mail=re1_mail.test(input_mail.val());

  var re2_mail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  var is_email=re2_mail.test(input_mail.val());

  if((text_valid_mail==false || is_email==false) && input_mail.val()!="")
  {
     $('.wrap-form.login.info').after('<div id="error-form" class="error-form" onclick="close_error()"><div class="warp-error"><div class="button-error-close" onclick="close_error()">×</div><div class="img-error"></div><p class="error-text">Địa chỉ Email không đúng định dạng, vui lòng nhập lại</p></div></div>');
     $('input.email').val('');$('#error-form').click(function(){$('input.email').focus();});$('.button-error-close').click(function(){$('input.email').focus();});
     $('.save').css('pointer-events','none');
  }
  else if(text_valid_mail==true || is_email==true || input_mail.val()=="")
  {
    $('.save').css('pointer-events','');
  }
})
        $('.save').click(function(){
                 var null_input="";
               if( $('.password').val()=="" )
              {
                        null_input+="Mật khẩu cũ, ";
              }
               if( $('input.new-password').val()=="" )
              {
                        null_input+="Mật khẩu mới, ";
              }
               if(  $('input.confirm-password').val()=="" )
              {
                        null_input+="Mật khẩu xác nhận, ";
              }
              if( null_input!="")
           {  null_input=null_input.slice (0, -2);
                        $('.wrap-form.login.info').after('<div id="error-form" class="error-form" onclick="close_error()"><div class="warp-error"><div class="button-error-close" onclick="close_error()">×</div><div class="img-error"></div><p class="error-text">'+null_input+' không được để trống</p></div></div>');
              if( $('input.confirm-password').val()=="" )
              {
                        $('#error-form').click(function(){ $('input.confirm-password').focus(); });$('.button-error-close').click(function(){ $('input.confirm-password').focus(); });
              }
              if( $('input.new-password').val()=="")
              {
                        $('#error-form').click(function(){ $('input.new-password').focus(); });$('.button-error-close').click(function(){ $('input.new-password').focus(); });
              }
              if( $('.password').val()=="")
              {
                $('#error-form').click(function(){ $('.password').focus(); });$('.button-error-close').click(function(){ $('.password').focus(); });
              }
             }
            else if($('input.confirm-password').val() != $('input.new-password').val())
            {
               $('.wrap-form.login.info').after('<div id="error-form" class="error-form" onclick="close_error()"><div class="warp-error"><div class="button-error-close" onclick="close_error()">×</div><div class="img-error"></div><p class="error-text">Mật khẩu mới và mật khẩu xác nhận không khớp</p></div></div>');
               $('#error-form').click(function(){ $('input.confirm-password').focus(); });$('.button-error-close').click(function(){ $('input.confirm-password').focus(); });
            }
                else
                {
                  var userid="<?= $user_id ?>";
                  var oldpass=$('.password').val();
                  var newpass=$('.new input').val();
                  var email=$('input.email').val();
                 $.ajax({
                    type : "post", 
                    dataType : "json", 
                    url : '<?php echo admin_url('admin-ajax.php');?>', 
                    data : {
                        action: "change_new_pass", 
                        userid: userid,
                        oldpass: oldpass,
                        newpass: newpass,
                        email: email,
                    },
                    context: this,
                    beforeSend: function(){
                    },
                    success: function(response) {
                        if(response.success)
                    {
                         if(response.data.indexOf('data-error="login-failed"')> -1)
                        {
                            $('.wrap-form.login.info').after('<div id="error-form" class="error-form" onclick="close_error()"><div class="warp-error"><div class="button-error-close" onclick="close_error()">×</div><div class="img-error"></div><p class="error-text">Mật khẩu cũ không chính xác vui lòng kiểm tra lại</p></div></div>');
                            $('.password').val('');$('#error-form').click(function(){ $('.password').focus(); });$('.button-error-close').click(function(){ $('.password').focus(); });
                        }
                        else
                        {
                        $('.wrap-form.login.info').after('<div id="error-form" class="error-form"><div class="warp-error"><div class="button-error-close">×</div><div class="img-error"></div><p class="error-text">Thay đổi thông tin thành công</p></div></div>');
                        $('.wrap-form.login.info').after(response.data);
                        $('#error-form').click(function(){ if($('#wp-submit').length>0){ $('#wp-submit').click();} else { location.reload();} });
                        $('.button-error-close').click(function(){  if($('#wp-submit').length>0){ $('#wp-submit').click();} else { location.reload();} });
                        }
                    }
                    else {
                            alert('Đã có lỗi xảy ra');
                        }
                    },
                    error: function(){
                    }
                })
                }
        });
      })
    </script>
<?php }
add_shortcode( 'change_password_subscriber', 'change_password_subscriber' );

/*Trang đổi pass employer*/
function change_password_employer()
{    $user = wp_get_current_user();
     $user_id=$user->ID;
  ?>
  <div class="wrap-form login info employer">
     <h1 class="title-form-contain">Change Password</h1>
        <div class="old w100">
            <label class="old-password">Password</label>
            <input type="password" name="password"  class="input password" value="" autocomplete="off" />
        </div>
        <div class="new w50">
            <label class="new-password ">New Password</label>
            <input type="password" name="new-password" class="input new-password"  value="" autocomplete="off" placeholder="Nhập new password" />
        </div>
        <div class="confirm w50">
            <label class="confirm-password ">Confirm Password</label>
            <input type="password" name="confirm-password" class="input confirm-password"   value="" autocomplete="off" placeholder="Confirm password"/>
        </div>
        <p class="wrap-button">
           <a class="save ">SAVE</a>
        </p>
    </div>
    <script type="text/javascript">
      function close_error() {
  var element = document.getElementById("error-form");
  element.classList.add("none");}
      jQuery(function($){
        $(document).keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
       $('.save').click();   
    }
});
        $('.save').click(function(){
                 var null_input="";
               if( $('.password').val()=="" )
              {
                        null_input+="Mật khẩu cũ, ";
              }
               if( $('input.new-password').val()=="" )
              {
                        null_input+="Mật khẩu mới, ";
              }
               if(  $('input.confirm-password').val()=="" )
              {
                        null_input+="Mật khẩu xác nhận, ";
              }
              if( null_input!="")
           {  null_input=null_input.slice (0, -2);
                        $('.wrap-form.login.info').after('<div id="error-form" class="error-form" onclick="close_error()"><div class="warp-error"><div class="button-error-close" onclick="close_error()">×</div><div class="img-error"></div><p class="error-text">'+null_input+' không được để trống</p></div></div>');
              if( $('input.confirm-password').val()=="" )
              {
                        $('#error-form').click(function(){ $('input.confirm-password').focus(); });$('.button-error-close').click(function(){ $('input.confirm-password').focus(); });
              }
              if( $('input.new-password').val()=="")
              {
                        $('#error-form').click(function(){ $('input.new-password').focus(); });$('.button-error-close').click(function(){ $('input.new-password').focus(); });
              }
              if( $('.password').val()=="")
              {
                $('#error-form').click(function(){ $('.password').focus(); });$('.button-error-close').click(function(){ $('.password').focus(); });
              }
             }
            else if($('input.confirm-password').val() != $('input.new-password').val())
            {
               $('.wrap-form.login.info').after('<div id="error-form" class="error-form" onclick="close_error()"><div class="warp-error"><div class="button-error-close" onclick="close_error()">×</div><div class="img-error"></div><p class="error-text">Mật khẩu mới và mật khẩu xác nhận không khớp</p></div></div>');
               $('#error-form').click(function(){ $('input.confirm-password').focus(); });$('.button-error-close').click(function(){ $('input.confirm-password').focus(); });
            }
                else
                {
                  var userid="<?= $user_id ?>";
                  var oldpass=$('.password').val();
                  var newpass=$('.new input').val();
                 $.ajax({
                    type : "post", 
                    dataType : "json", 
                    url : '<?php echo admin_url('admin-ajax.php');?>', 
                    data : {
                        action: "change_new_pass", 
                        userid: userid,
                        oldpass: oldpass,
                        newpass: newpass,
                    },
                    context: this,
                    beforeSend: function(){
                    },
                    success: function(response) {
                        if(response.success)
                    {
                         if(response.data.indexOf('data-error="login-failed"')> -1)
                        {
                            $('.wrap-form.login.info').after('<div id="error-form" class="error-form" onclick="close_error()"><div class="warp-error"><div class="button-error-close" onclick="close_error()">×</div><div class="img-error"></div><p class="error-text">Mật khẩu cũ không chính xác vui lòng kiểm tra lại</p></div></div>');
                            $('.password').val('');$('#error-form').click(function(){ $('.password').focus(); });$('.button-error-close').click(function(){ $('.password').focus(); });
                        }
                        else
                        {
                        $('.wrap-form.login.info').after('<div id="error-form" class="error-form"><div class="warp-error"><div class="button-error-close">×</div><div class="img-error"></div><p class="error-text">Thay đổi thông tin thành công, vui lòng đăng nhập lại</p></div></div>');
                        $('.wrap-form.login.info').after(response.data);
                        $('.button-error-close').click(function(){ $('#error-form').remove(); window.location.href ="<?= $_SESSION['url_last_page_head'] ?>"; });
                        $('#error-form').click(function(){ $(this).remove();window.location.href ="<?= $_SESSION['url_last_page_head'] ?>"; });
                        }
                    }
                    else {
                            alert('Đã có lỗi xảy ra');
                        }
                    },
                    error: function(){
                    }
                })
                }
        });
      })
    </script>
<?php }
add_shortcode( 'change_password_employer', 'change_password_employer' );

/*ajax đổi pass*/
function change_new_pass() {
    ob_start();
    $user_id =  (isset($_POST['userid']))?esc_attr($_POST['userid']) : '';
    $old_pass =  (isset($_POST['oldpass']))?esc_attr($_POST['oldpass']) : '';
    $user_pass =  (isset($_POST['newpass']))?esc_attr($_POST['newpass']) : '';
    $user_email =  (isset($_POST['email']))?esc_attr($_POST['email']) : '';
    $user = get_user_by( 'id', $user_id );
    $username= $user->display_name;
    $old_password=$user->user_pass;
    if(!wp_check_password( $old_pass, $old_password, $user_id ))
    {
      echo 'data-error="login-failed"';
    }
    else{
    if($user_email!=""){ wp_update_user( array( 'ID' => $user_id, 'user_email' => $user_email ) ); }
    if($user_pass!=""){ wp_set_password ($user_pass , $user_id ); 
    $form = '
        <form style="height:0px;overflow:hidden;" name="login-form" id="login-form" action="' . esc_url( site_url( 'wp-login.php', 'login_post' ) ) . '" method="post">
                <input type="text" name="log" id="user_login" class="input" value="' . esc_attr( $username ) . '"  />
                <input type="password" name="pwd" id="user_pass" class="input" value="' . esc_attr( $user_pass ) . '"  />
                <input name="rememberme" type="checkbox" id="rememberme" value="forever" checked="checked"/>
                <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary"/>
        </form>';
    echo $form; }
    }
    $result = ob_get_clean();
    wp_send_json_success($result); 
    die();
}
add_action( 'wp_ajax_change_new_pass', 'change_new_pass' );
add_action( 'wp_ajax_nopriv_change_new_pass', 'change_new_pass' );

/*trang job dashboard*/
function job_dashboard_code()
{
if( !is_user_logged_in() ) { ?>
      <script type="text/javascript">
        jQuery(function($){
            window.location.href="<?= get_home_url(); ?>/login-for-recruiter/";
        })
      </script>
  <?php  
} 
 elseif( is_user_logged_in() ) 
 {  
    $user = wp_get_current_user();
       $user_roles=$user->roles;
       if($user_roles[0]!="subscriber")
       { 
        if(isset($_GET['pages']) && $_GET['pages']=='company-profile' ){ echo do_shortcode('[company_profile]'); }
        else if(isset($_GET['pages']) && $_GET['pages']=='post-a-new-job' ){ echo do_shortcode('[submit_job_form_code]'); }
        else if(isset($_GET['pages']) && $_GET['pages']=='change-password' ){ echo do_shortcode('[change_password_employer]'); }
        else if(isset($_GET['pages']) && $_GET['pages']=='edit-job' ){ echo do_shortcode('[edit_job_new]'); }
        else { echo do_shortcode('[job_dashboard_new]'); }
       }
       else
       { ?>
         <script type="text/javascript">
        jQuery(function($){
            window.location.href="<?= get_home_url(); ?>/book-mark/";
        })
      </script>
      <?php }
 }
}
add_shortcode( 'job_dashboard_code', 'job_dashboard_code' );
/*job_dashboard_new*/
function job_dashboard_new()
{ ?>
<?php 
$user = wp_get_current_user();
$args=array(
'post_type'=>'job_listing',
'post_status'=> array('any','expired'),
'author' => $user->ID,
'nopaging' => true,);
$post_query_count = new WP_Query( $args );
$count_post=0;
$count_active=0;
 while ( $post_query_count->have_posts() ) {  $post_query_count->the_post(); if(get_post_status()=="publish"){$count_active++;} $count_post++; } 
 wp_reset_query();
$url_current= home_url( '/job-dashboard/' );
$args=array(
'post_type'=>'job_listing',
'post_status'=> array('any','expired'),
'author' => $user->ID,
'posts_per_page' => 10,
'paged' => get_query_var('paged'),              
'orderby' => 'modified',
'order' => 'DESC');
$count=0;
$post_query = new WP_Query( $args );
?>      
        <div class="wrap-form login info space-between">
        <h1 class="title-form-contain w100">Job Management</h1>
        <div class="list-count">
        <div class="count-job-posted"><b><?=$count_post?></b>  Job Posted</div>
        <div class="count-job-active"><b><?=$count_active?></b>  Active Jobs</div>
        </div>
        <div class="table-list desktop">
        <div class="wrap-header"><div class="head-1">Title</div><div class="head-2">Created Date</div><div class="head-3">Expired Date</div><div class="head-4">Status</div><div class="head-5">Action</div></div>
          <?php  while ( $post_query->have_posts() ) { 
                $post_query->the_post();
              $post_id=get_the_ID(); ?>
        <div class="wrap-line-job">
        <div class="col-1"><div class="job-title"><?= get_the_title(); ?></div><?= jobhunt_template_job_listing_location() ?></div>
        <div class="col-2"><?= get_the_date('d/m/Y') ?></div>
        <div class="col-3"><?php if(get_post_meta( $post_id, '_job_expires', true )!=""){ echo date("d/m/Y", strtotime(get_post_meta( $post_id, '_job_expires', true ))); } else { echo '<div class="null"></div>'; } ?></div>
        <?php if(get_post_status()=="publish"){ echo "<div class='col-4' style='color:#43CBB5'>Active</div>"; } elseif(get_post_status()=="expired") { echo "<div class='col-4' style='color:#E34343'>expired</div>"; } else  {echo "<div class='col-4' style='color:#FF7A00'>".get_post_status()."</div>"; } ?>
        <div class="col-5"><a class="job-link" href="<?php if(get_post_status()=='publish'): echo get_permalink(); else : echo get_preview_link(get_post($post_id));  endif;?>"></a><a class="job-edit" href="<?= $url_current.'?pages=edit-job&id='.$post_id.''?>"></a><a class="job-delete" data-id="<?= $post_id ?>"></a></div>
        </div> <?php $count++; }
 ?>
    </div>
    <div class="table-list tablet">
          <?php  while ( $post_query->have_posts() ) { 
                $post_query->the_post();
              $post_id=get_the_ID(); ?>
        <div class="wrap-line-job">
        <div class="col-1"><div class="head-1">Title</div><div class="job-title"><?= get_the_title(); ?><?= jobhunt_template_job_listing_location() ?></div></div>
        <div class="col-2"><div class="head-2">Created Date</div><?= get_the_date('d/m/Y') ?></div>
        <div class="col-3"><div class="head-3">Expired Date</div><?php if(get_post_meta( $post_id, '_job_expires', true )!=""){ echo date("d/m/Y", strtotime(get_post_meta( $post_id, '_job_expires', true ))); } else { echo '<div class="null"></div>'; } ?></div>
        <?php if(get_post_status()=="publish"){ echo "<div class='col-4'><div class='head-4'>Status</div><div style='color:#43CBB5'>Active</div></div>"; } elseif(get_post_status()=="expired") { echo "<div class='col-4'><div class='head-4'>Status</div><div style='color:#E34343'>Expired</div></div>"; } else  {echo "<div class='col-4'><div class='head-4'>Status</div><div style='color:#FF7A00'>".get_post_status()."</div></div>"; } ?>
        <div class="col-5"><div class="head-5">Action</div><a class="job-link" href="<?php if(get_post_status()=='publish'): echo get_permalink(); else : echo get_preview_link(get_post($post_id));  endif;?>"></a><a class="job-edit" href="<?= $url_current.'?pages=edit-job&id='.$post_id.''?>"></a><a class="job-delete" data-id="<?= $post_id ?>"></a></div>
        </div> <?php $count++; }
 ?>
    </div>
  <nav class="job-manager-pagination">
    <?php
    $big = 999999999;
    echo paginate_links( array(
      'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
      'format' => '?paged=%#%',
      'prev_text'    => __('←'),
      'next_text'    => __('→'),
      'current' => max( 1, get_query_var('paged') ),
      'total' => $post_query->max_num_pages,
                    'end_size'     => 2,
                    'mid_size'     => 2,
      ) );
      ?>
  </nav>
  <script type="text/javascript">
jQuery(function($){
         $('.job-delete').click(function(){ 
          var jobid=$(this).attr('data-id');
            $.ajax({
                    type : "post", 
                    dataType : "json", 
                    url : '<?php echo admin_url('admin-ajax.php');?>', 
                    data : {
                        action: "job_delete", 
                        jobid: jobid,
                    },
                    context: this,
                    beforeSend: function(){
                    },
                    success: function(response) {
                        if(response.success)
                        {
                          if(response.data=="ok")
                          { 
                            location.reload();
                          }
                        }
                        else {
                            alert('Đã có lỗi xảy ra');
                        }
                    },
                    error: function(){
                    }
                });
            });
        });
</script>
<?php wp_reset_query();  } 
add_shortcode('job_dashboard_new','job_dashboard_new');
/*job edit*/
function edit_job_new()
{
$post_id = $_GET['id'];
?>
       <form id="new_post" class="wrap-form login info space-between post-job " method="post" action="" >
        <h1 class="title-form-contain w100">Edit Job</h1>
        <div class="form-group-job-title w100">
            <label for="job_title">Job Title</label>
            <input type="text" name="job_title" class="form-control" placeholder="Tiêu đề" value="<?= get_the_title( $post_id ) ?>">
        </div>
        <div class="form-group-job-description w100">
          <label for="job_content">Job Description </label>
          <?php wp_editor( get_post($post_id)->post_content, 'job-description', array( 'textarea_name' => 'job_content' ));?>
        </div>
        <div class="form-group-job-requirement w100">
          <label for="job_requirement">Job Requirement </label>
          <?php wp_editor( get_post_meta( $post_id, 'job_requirement', true ) , 'job_requirement', array( 'textarea_name' => 'job_requirement' ));?>
        </div>
        <div class="form-group-job-type w50">
          <label for="job_listing_type">Job Type</label>
         <?php drop_taxonomy2('job_listing_type','-- select job type --', $post_id); ?>       
        </div>

         <div class="form-group-job-category w50 more-option">
          <label for="job_listing_category">Category</label>
          <ul class="ul_job_listing_category">
        <?php $val_job_listing_category=wp_get_post_terms( $post_id, 'job_listing_category', array( 'fields' => 'names' ) ); $val_lo_input="";
        foreach ($val_job_listing_category as $value => $val): $val_lo_input.=$val.', ';
        ?>
        <li class="li_job_listing_category" title="<?= $val ?>"><p class="text-val"><?= $val ?></p><p class="li-value-delete">×</p></li>
        <?php endforeach; ?>
        <li class="li_job_listing_category_last"><input class="li-input-text-show_job_listing_category" type="text" data-for="job_listing_category"></li>
        </ul>
          <input type="hidden" name="job_listing_category_input" class="job-listing-category" value="<?= $val_lo_input ?>" />
          <div class="job_listing_category_autocomplete" style="display: none;">
            <?php $terms=get_terms(['taxonomy' => 'job_listing_category','hide_empty' => false,]); 
            foreach ( $terms as $terms_name) : ?>
              <div class="autocomplete_text" style="display: none"><?=$terms_name->name?></div>
            <?php endforeach;?>
              <div class="autocomplete_text not_found" style="display: none">No Category found</div> 
          </div>  
        </div>

        <div class="form-group-job-career-level w50">
          <label for="job_listing_career_level">Level</label>
         <?php drop_taxonomy2('job_listing_career_level','-- select job career level --', $post_id); ?>       
        </div>

        <div class="form-group-job-industry w50 more-option">
          <label for="job_listing_industry">Industry</label>
        <ul class="ul_job_listing_industry">
        <?php $val_job_listing_industry=wp_get_post_terms( $post_id, 'job_listing_industry', array( 'fields' => 'names' ) ); $val_lo_input="";
        foreach ($val_job_listing_industry as $value => $val): $val_lo_input.=$val.', ';
        ?>
        <li class="li_job_listing_industry" title="<?= $val ?>"><p class="text-val"><?= $val ?></p><p class="li-value-delete">×</p></li>
        <?php endforeach; ?>
        <li class="li_job_listing_industry_last"><input class="li-input-text-show_job_listing_industry" type="text" data-for="job_listing_industry"></li>
        </ul>
          <input type="hidden" name="job_listing_industry_input" class="job-listing-industry" value="<?= $val_lo_input ?>" />
          <div class="job_listing_industry_autocomplete" style="display: none;">
            <?php $terms=get_terms(['taxonomy' => 'job_listing_industry','hide_empty' => false,]); 
            foreach ( $terms as $terms_name) : ?>
              <div class="autocomplete_text" style="display: none"><?=$terms_name->name?></div>
            <?php endforeach;?>
              <div class="autocomplete_text not_found" style="display: none">No Industry found</div> 
          </div>     
        </div>

        <div class="form-group-job-experience w50">
          <label for="job_listing_experience">Experience</label>
         <?php drop_taxonomy2('job_listing_experience','-- select job experience --', $post_id); ?>       
        </div>
        <div class="form-group-job-qualification w50">
          <label for="job_listing_qualification">Qualification</label>
         <?php drop_taxonomy2('job_listing_qualification','-- select job qualification --', $post_id); ?>       
        </div>

        <div class="form-group-job-location-city w100 more-option">
          <label for="job_location_city">Work Location</label>
        <ul class="ul_job_location_city">
        <?php $val_job_location_city=wp_get_post_terms( $post_id, 'job_location_city', array( 'fields' => 'names' ) ); $val_lo_input="";
        foreach ($val_job_location_city as $value => $val): $val_lo_input.=$val.', ';
        ?>
        <li class="li_job_location_city" title="<?= $val ?>"><p class="text-val"><?= $val ?></p><p class="li-value-delete">×</p></li>
        <?php endforeach; ?>
        <li class="li_job_location_city_last"><input class="li-input-text-show_job_location_city" type="text" data-for="job_location_city"></li>
        </ul>
          <input type="hidden" name="job_location_city_input" class="job-location-city" value="<?= $val_lo_input ?>" />
          <div class="job_location_city_autocomplete" style="display: none;">
            <?php $terms=get_terms(['taxonomy' => 'job_location_city','hide_empty' => false,]); 
            foreach ( $terms as $terms_name) : ?>
              <div class="autocomplete_text" style="display: none"><?=$terms_name->name?></div>
            <?php endforeach;?>
              <div class="autocomplete_text not_found" style="display: none">No Work Location found</div>
          </div> 
        </div>

        <div class="form-group-job-skill-requirement w100">
          <label for="skill_requirement">Skill requirement</label>
          <input type="text" name="skill_requirement" class="job-skill-requirement" value="<?= get_post_meta( $post_id, 'skill_requirement',  true ) ?>" />
        </div>
       <div class="form-group-job-expires-datepicker w100">
          <label for="_job_expires-datepicker">Application deadline</label>
         <select class="search_category" name="_job_expires-datepicker" style="color:#555">
            <option value="7d" style="color:#555" selected>Expired in 7 days</option>
            <option value="15d" style="color:#555">Expired in 15 days</option>
            <option value="30d" style="color:#555">Expired in 30 days</option>
         </select>      
        </div>
        <input type="hidden" name="update_post" value="<?= $post_id ?>" />
        <?php wp_nonce_field( 'post_nonce', 'post_nonce_field' ); ?>
        <div class="form-group">
            <div class="col-sm-12" style="padding-left:0;">
              <button type="submit" class="btn btn-primary save">Save</button><a class="btn btn-primary cancel" onclick='window.location.href ="<?= home_url( '/job-dashboard/' ); ?>"'>Cancel</a>
            </div>
        </div>
    </form>
    <script type="text/javascript">jQuery(function($){
      function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
}
      function out_click(name)
        {
          $(document).on('click', function (e) 
          {
            if ($(e.target).closest('.'+name+'_autocomplete').length === 0) 
            {
              $('.'+name+'_autocomplete').hide();
            }
          });
        }
        out_click('job_location_city');
        out_click('job_listing_industry');
        out_click('job_listing_category');
      function focus(name)
        {
          $('ul.ul_'+name).click(function(){ $('input.li-input-text-show_'+name).focus(); });
        }
        focus('job_location_city');
        focus('job_listing_industry');
        focus('job_listing_category');
      function search(name)
        {  
          $('input.li-input-text-show_'+name).on("change paste keyup", function() 
          {
            if($(this).val()!="")
              {
                var text=xoa_dau($(this).val());
                count_found=0;
                text_input_show=new RegExp('^'+text+'.*', 'i');
                $('.'+name+'_autocomplete .autocomplete_text').each(function()
                  {  
                    var text_option=$(this).text();
                    if( text_input_show.test(text_option) )
                      { 
                        $(this).css('display','block');
                        $('.'+name+'_autocomplete').css('display','block');
                        count_found++;
                      }
                    else 
                      {
                        $(this).css('display','none');
                      }
                  });
                if(count_found==0)
                  { 
                    $('.'+name+'_autocomplete .autocomplete_text.not_found').css('display','block'); 
                    $('.'+name+'_autocomplete').css('display','block');
                  }
              }
            else
              {
                $('.'+name+'_autocomplete').css('display','none');
              }
          });
        search_click(name);
        }
        search('job_location_city');
        search('job_listing_industry');
        search('job_listing_category');
        function search_click(name)
        {
          $('div.more-option .'+name+'_autocomplete .autocomplete_text').click(function()
            {
              var input_value=$('input[name="'+name+'_input"]').val();
              if(input_value.indexOf($(this).text())== -1 )
                {
                  $('input[name="'+name+'_input"]').val(input_value+=$(this).text()+', ');
                  $('.li_'+name+'_last').before('<li class="li_'+name+'" title="'+$(this).text()+'"><p class="text-val">'+$(this).text()+'</p><p class="li-value-delete">×</p></li>');
                  $(this).css('display','none');
                  xoa_li(name);
                }
              else if(input_value.indexOf($(this).text())> -1 )
                {   
                  var val_dele=$(this).text()+', ';
                  var new_val=input_value.replaceAll(val_dele,"");
                  $('input[name="'+name+'_input"]').val(new_val);
                  $('li.li_'+name+'').each(function() {if( val_dele.indexOf($(this).text().replace("×",""))>-1) $(this).remove()});
                  $(this).css('display','none');
                }
                  $('.'+name+'_autocomplete').css('display','none');
                  $(this).parents('div.more-option').find('.li-input-text-show_'+name+'').val('');
                  $(this).parents('div.more-option').find('.li-input-text-show_'+name+'').focus();
          })
        }
        function xoa_li(name)
        {
            $('p.li-value-delete').click(function(e)
            { 
              var text_del=$(this).parents('li').text().replace("×","")+', '; 
              var new_val=$('input[name="'+name+'_input"]').val().replaceAll(text_del,"");
              $('input[name="'+name+'_input"]').val(new_val);
              $(this).parents('li').remove();
            });
        }
        xoa_li('job_location_city');
        xoa_li('job_listing_industry');
        xoa_li('job_listing_category');
})
    </script>
<?php 
if( $_SERVER['REQUEST_METHOD'] == 'POST' && !empty( $_POST['update_post'] ) && isset( $_POST['post_nonce_field'] ) && wp_verify_nonce( $_POST['post_nonce_field'], 'post_nonce' )) {

if (isset($_POST['job_title'])) {
    $job_title = $_POST['job_title'];
}
if (isset($_POST['job_content'])) {
    $job_content = $_POST['job_content'];
}
if (isset($_POST['job_requirement'])) {
    $job_requirement = $_POST['job_requirement'];
}
if (isset($_POST['job_listing_type'])) {
    $job_listing_type = $_POST['job_listing_type'];
}
if (isset($_POST['job_listing_category_input'])) {
    $job_listing_category[] = explode(", ",$_POST['job_listing_category_input']);
}
if (isset($_POST['job_listing_career_level'])) {
    $job_listing_career_level = $_POST['job_listing_career_level'];
}
if (isset($_POST['job_listing_industry_input'])) {
    $job_listing_industry[] = explode(", ",$_POST['job_listing_industry_input']);
}
if (isset($_POST['job_listing_experience'])) {
    $job_listing_experience = $_POST['job_listing_experience'];
}
if (isset($_POST['job_listing_qualification'])) {
    $job_listing_qualification = $_POST['job_listing_qualification'];
}
if (isset($_POST['job_location_city_input'])) {
    $job_location_city[] = explode(", ",$_POST['job_location_city_input']);
}
if (isset($_POST['skill_requirement'])) {
    $skill_requirement = $_POST['skill_requirement'];
}
if (isset($_POST['_job_expires-datepicker'])) {
  if($_POST['_job_expires-datepicker']=="7d")
  {
    $_job_expires = time()+ 604800;
  }
  elseif($_POST['_job_expires-datepicker']=="15d")
  {
    $_job_expires = time()+ 1296000;
  }
  else
  {
    $_job_expires = time()+ 2592000;
  }
}
if($_POST['job_title']!="")
{
$post_id = $_POST['update_post'];
//
$location=array();
foreach ($job_location_city[0] as $location_name) {
$location[]=get_term_by('name', $location_name, 'job_location_city')->term_id;
}
wp_set_post_terms( $post_id, $location , 'job_location_city' );
$cate=array();
foreach ($job_listing_category[0] as $cate_name) {
$cate[]=get_term_by('name', $cate_name, 'job_listing_category')->term_id;
}
wp_set_post_terms( $post_id, $cate , 'job_listing_category' );
$industry=array();
foreach ($job_listing_industry[0] as $industry_name) {
$industry[]=get_term_by('name', $industry_name, 'job_listing_industry')->term_id;
}
wp_set_post_terms( $post_id, $industry , 'job_listing_industry' );
//
update_post_meta( $post_id, 'job_requirement', $job_requirement );
update_post_meta( $post_id, '_job_expires', date("Y-m-d", $_job_expires) );
update_post_meta( $post_id, 'skill_requirement',  $skill_requirement );
//
wp_set_post_terms( $post_id, array(get_term_by('name', $job_listing_type, 'job_listing_type')->term_id), 'job_listing_type' );
wp_set_post_terms( $post_id, array(get_term_by('name', $job_listing_career_level, 'job_listing_career_level')->term_id), 'job_listing_career_level' );
wp_set_post_terms( $post_id, array(get_term_by('name', $job_listing_experience, 'job_listing_experience')->term_id), 'job_listing_experience' );
wp_set_post_terms( $post_id, array(get_term_by('name', $job_listing_qualification, 'job_listing_qualification')->term_id), 'job_listing_qualification' );
wp_update_post( array('ID'=> $post_id,'post_content' => $job_content,) );

?>
<script type="text/javascript">
  jQuery(function($){
     $('.wrap-form.login.info').after('<div id="error-form" class="error-form"><div class="warp-error"><div class="button-error-close">×</div><div class="img-error"></div><p class="error-text">Cập nhật job thành công</p></div></div>');
     $('.button-error-close').click(function(){ $('#error-form').remove(); window.location.href ="<?= $_SESSION['url_last_page_head'] ?>"; });
     $('#error-form').click(function(){ $(this).remove();window.location.href ="<?= $_SESSION['url_last_page_head'] ?>"; });
  })
</script>
<?php
}
}
}
add_shortcode( 'edit_job_new', 'edit_job_new' );
/*job delete*/
function job_delete()
{   ob_start();
    $job_id =  (isset($_POST['jobid']))?esc_attr($_POST['jobid']) : '';
    if(wp_delete_post ( $job_id ,true ))
    {
      wp_delete_post ( $job_id ,true );
        echo 'ok';
    }
    else 
    {
      echo 'bug';
    }
    $result = ob_get_clean();
    wp_send_json_success($result); 
    die();
}
add_action( 'wp_ajax_job_delete', 'job_delete' );
add_action( 'wp_ajax_nopriv_job_delete', 'job_delete' );

/*trang thông tin công ty*/
function company_profile()
{
     $user = wp_get_current_user();
     $meta=get_user_meta( $user->ID );
     $company_name=$meta['company_name'][0];
     $company_logo_url=get_the_post_thumbnail_url( $meta['id_company'][0], 'full' );
     $phone_number=$meta['phone_number'][0];
     $email=$user->user_email;
     $city_name=$meta['city_name'][0];
     $address=$meta['district_name'][0];
  ?>
  <div class="wrap-form login info space-between">
     <h1 class="title-form-contain w100">Company Profile</h1>
        <div class="w100 grid-company">
            <div class="job-listing-company-logo"><div class="button-error-close">×</div><img class="company_logo" src="<?= $company_logo_url ?>" alt="<?= $company_name ?>"></div>
            <div class="wrap-button-change">
            <input type="button" id="image-upload-button" class="button button-secondary" value="Browse" /> 
            <div class="description-text">Max file size is 1MB, Minimum dimension: 270x210 And Suitable files are .jpg & .png</div>
            </div>
            <script type="text/javascript">jQuery(function($){
              var mediaUploader ;
              $('.grid-company .button-error-close').click(function(e){$('.grid-company .company_logo').attr('src','');});
              $('.grid-company .company_logo').click(function(e){$('#image-upload-button').click();});
              jQuery('#image-upload-button').click(function(e){
    e.preventDefault();
    if (mediaUploader){
        mediaUploader.open();
        return;
    }    
    mediaUploader = wp.media.frames.file_frame = wp.media({
    library: false,
    title: 'Chọn ảnh',
    button:{
        text: 'Chọn ảnh này'
    },
    multiple: false
    }); 
  mediaUploader.on('select', function(){
    attachment = mediaUploader.state().get('selection').first().toJSON();
    $('.company_logo').attr('src', attachment.url);
    $('.company_logo').attr('src-id', attachment.id);
  });
  mediaUploader.open();
  });
            })</script>
        </div>
        <div class="w100">
            <label class="label-email">Company Name</label>
            <input type="text" name="company_name" id="pass1" class="input company-name" value="<?= $company_name?>" autocomplete="off" placeholder="Nhập tên công ty"/>
        </div>
        <div class="w100 hidden"></div>
        <div class="w50">
            <label class="old-password">Phone Number</label>
            <input type="text" name="phone_number"  class="input phone-number" value="<?= $phone_number ?>" autocomplete="off" placeholder="Nhập phone number"/>
        </div>
        <div class="w50">
            <label class="new-password">Email</label>
            <input type="text" name="user_email" class="input user-email"  value="<?= $email ?>" autocomplete="off" placeholder="Nhập địa chỉ email" />
        </div>
        <div class="w50">
            <label class="confirm-password">City</label>
            <select class="input city-name search_category" name="city_name">
                            <option value="" style="color:#a7a7a7">--Chọn tỉnh thành--</option>
                           <?php
$category_select=array();
$terms2=get_terms([
    'taxonomy' => 'company_location_city',
    'hide_empty' => false,
]); foreach ( $terms2 as $terms_name2 ) : $category_select[]=$terms_name2->name;  endforeach; 
?>
                            <?php foreach ( $category_select as $category) : $match=0;
                              if($category==$city_name){$match=1;}
                            ?>
                            <option value="<?= $category ?>" style="color:#555555" <?= ($match==1) ? 'selected' : '' ?> ><?= $category ?></option>
                            <?php  endforeach; ?>
 </select>
        </div>
        <div class="w50">
            <label class="confirm-password">Address</label>
            <input type="text" name="district_name" class="input district-name"   value="<?= $address ?>" autocomplete="off" placeholder="Nhập địa chỉ"/>
        </div>
        <p class="wrap-button">
           <a class="edit">EDIT</a><a class="save none">SAVE</a><a class="cancel none">CANCEL</a>
        </p>
    </div>
    <script type="text/javascript">
      function close_error() {
  var element = document.getElementById("error-form");
  element.classList.add("none");}
      jQuery(function($){
            $(".phone-number").on("focusout", function() {
   var input_phone=$(this);
  var re1_phone = /^[0-9#*+-]*$/;
  var text_valid_phone=re1_phone.test(input_phone.val());

  if(text_valid_phone==false || input_phone.val().length<9 && input_phone.val()!="")
  {
    $('.wrap-form.login.info').after('<div id="error-form" class="error-form" onclick="close_error()"><div class="warp-error"><div class="button-error-close" onclick="close_error()">×</div><div class="img-error"></div><p class="error-text">Số điện thoại không đúng định dạng, vui lòng nhập lại</p></div></div>');
     $('.phone-number').val('');$('#error-form').click(function(){$('.phone-number').focus();});$('.button-error-close').click(function(){$('.phone-number').focus();});
     $('.save').css('pointer-events','none');
  }
  else if(text_valid_phone==true || input_phone.val().length>8 && input_phone.val()!="")
  {
     $('.save').css('pointer-events','');
  }
});
         $(".user-email").on("focusout", function() {
   var input_mail=$(this);
  var re1_mail = /^[a-zA-Z0-9@.!#$%&'*+/\\=?^_`{|}~-]*$/;
  var text_valid_mail=re1_mail.test(input_mail.val());

  var re2_mail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  var is_email=re2_mail.test(input_mail.val());

  if((text_valid_mail==false || is_email==false) && input_mail.val()!="")
  {
     $('.wrap-form.login.info').after('<div id="error-form" class="error-form" onclick="close_error()"><div class="warp-error"><div class="button-error-close" onclick="close_error()">×</div><div class="img-error"></div><p class="error-text">Địa chỉ Email không đúng định dạng, vui lòng nhập lại</p></div></div>');
     $('.user-email').val('');$('#error-form').click(function(){$('.user-email').focus();});$('.button-error-close').click(function(){$('.user-email').focus();});
     $('.save').css('pointer-events','none');
  }
  else if((text_valid_mail==true || is_email==true) && input_mail.val()!="")
  {
    $('.save').css('pointer-events','');
  }
})
        $('.edit').click(function(){
          $(this).addClass('none'); $('.save').removeClass('none');$('.cancel').removeClass('none'); $('.confirm').removeClass('none'); $('.new').removeClass('none');$('input.email').val('');
          $(document).keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
       $('.save').click();   
    }
});  
        });
        $('.cancel').click(function(){
          location.reload(); 
        });
        $('.save').click(function(){
              var null_input="";
              if( $('.company_logo').attr('src')=="")
              {
                        null_input+="Logo công ty, ";
              }
               if( $('.company-name').val()=="")
              {
                        null_input+="Tên công ty, ";
              }
               if( $('.phone-number').val()=="" )
              {
                        null_input+="Số điện thoại, ";
              }
               if( $('.user-email').val()=="" )
              {
                        null_input+="Địa chỉ Email, ";
              }
               if(  $('.city-name').val()=="" )
              {
                        null_input+="Tên thành phố, ";
              }
              if( null_input!="")
           {  null_input=null_input.slice (0, -2);
                        $('.wrap-form.login.info').after('<div id="error-form" class="error-form" onclick="close_error()"><div class="warp-error"><div class="button-error-close" onclick="close_error()">×</div><div class="img-error"></div><p class="error-text">'+null_input+' không được để trống</p></div></div>');
                    if(  $('.city-name').val()=="" )
              {
                        $('#error-form').click(function(){ $('.city-name').focus(); });$('.button-error-close').click(function(){ $('.city-name').focus(); });
              }
              if( $('.user-email').val()=="" )
              {
                        $('#error-form').click(function(){ $('.user-email').focus(); });$('.button-error-close').click(function(){ $('.user-email').focus(); });
              }
              if( $('.phone-number').val()=="" )
              {
                        $('#error-form').click(function(){ $('.phone-number').focus(); });$('.button-error-close').click(function(){ $('.phone-number').focus(); });
              }
              if( $('.company-name').val()=="")
              {
                        $('#error-form').click(function(){ $('.company-name').focus(); });$('.button-error-close').click(function(){ $('.company-name').focus(); });
              }
              if( $('.company_logo').attr('src')=="")
              {
                $('#error-form').click(function(){ $('#image-upload-button').focus(); });$('.button-error-close').click(function(){ $('#image-upload-button').focus(); });
              }
             }
              else
           {
                  var userid="<?= $user->ID  ?>";
                    var logo_id=$('.company_logo').attr('src-id');
                    var company_name=$('.company-name').val();
                    var phone_number=$('.phone-number').val();
                    var user_email=$('.user-email').val();
                    var city_name=$('.city-name').val();
                  var district_name=$('.district-name').val();
                  var update=1;
                  function update_company(){
                    $.ajax({
                    type : "post", 
                    dataType : "json", 
                    url : '<?php echo admin_url('admin-ajax.php');?>', 
                    data : {
                        action: "change_company_profile", 
                        userid: userid,
                        logo_id: logo_id,
                        company_name: company_name,
                        phone_number: phone_number,
                        user_email: user_email,
                        city_name: city_name,
                        district_name: district_name
                    },
                    context: this,
                    beforeSend: function(){
                    },
                    success: function(response) {
                        if(response.success)
                        {
                        var data_error="";
                        if(response.data.indexOf('data-error="company_name"')> -1)
                        {
                            data_error+="Tên công ty, ";
                        }
                        if(response.data.indexOf('data-error="phone_number"')> -1)
                        {
                            data_error+="Số điện thoại, ";
                        }  
                         if(response.data.indexOf('data-error="user_email"')> -1)
                        {
                            data_error+="Địa chỉ Email, ";
                        }  
                        if(data_error!="" )
                    {
                        data_error=data_error.slice (0, -2);
                        $('.wrap-form.login.info').after('<div id="error-form" class="error-form" onclick="close_error()"><div class="warp-error"><div class="button-error-close" onclick="close_error()">×</div><div class="img-error"></div><p class="error-text">'+data_error+' đã tồn tại, vui lòng nhập lại</p></div></div>');
                         if(response.data.indexOf('data-error="user_email"')> -1)
                        {
                          $('.user-email').val('');$('#error-form').click(function(){$('.user-email').focus();});$('.button-error-close').click(function(){$('.user-email').focus();});
                        }  
                        if(response.data.indexOf('data-error="phone_number"')> -1)
                        {
                          $('.phone-number').val('');$('#error-form').click(function(){$('.phone-number').focus();});$('.button-error-close').click(function(){$('.phone-number').focus();});
                        } 
                        if(response.data.indexOf('data-error="company_name"')> -1)
                        {
                          $('.company-name').val('');$('#error-form').click(function(){$('.company-name').focus();});$('.button-error-close').click(function(){$('.company-name').focus();});
                        }
                    }
                     else if(update==1 && data_error=="")
                    {
                        update=2;
                        update_company();
                    }
                    else if(update==2 && data_error=="")
                    {
                        $('.wrap-form.login.info').after(response.data);
                        $('.button-error-close').click(function(){ $('#error-form').remove(); window.location.href ="<?= $_SESSION['url_last_page_head'] ?>"; });
                        $('#error-form').click(function(){ $(this).remove();window.location.href ="<?= $_SESSION['url_last_page_head'] ?>"; });
                    }
                        }
                        else {
                            alert('Đã có lỗi xảy ra');
                        }
                    },
                    error: function(){
                    }
                })
                  }
              update_company();
          $(this).addClass('none'); $('.cancel').addClass('none');$('.edit').removeClass('none');
             }
        });
      })
      function close_error() {
  var element = document.getElementById("error-form");
  element.classList.add("none");}
    </script>
<?php }
add_shortcode( 'company_profile', 'company_profile' );

function change_company_profile() {
    ob_start();
    $user_id =  (isset($_POST['userid']))?esc_attr($_POST['userid']) : '';
    $logo_id =  (isset($_POST['logo_id']))?esc_attr($_POST['logo_id']) : '';
    $company_name =  (isset($_POST['company_name']))?esc_attr($_POST['company_name']) : '';
    $phone_number =  (isset($_POST['phone_number']))?esc_attr($_POST['phone_number']) : '';
    $user_email =  (isset($_POST['user_email']))?esc_attr($_POST['user_email']) : '';
    $city_name =  (isset($_POST['city_name']))?esc_attr($_POST['city_name']) : '';
    $district_name =  (isset($_POST['district_name']))?esc_attr($_POST['district_name']) : '';
    $current_user=wp_get_current_user();
    $current_user_id=$current_user->ID;
    $users_all = get_users();
    $meta_current=get_user_meta( $current_user->ID );
    $company_id=$meta_current['id_company'][0];
    $bug=0;
     /*kiểm tra company_name*/
    foreach($users_all as $user)
    {
     if($user->ID!=$current_user_id)
        {
          $meta=get_user_meta( $user->ID );
          $company_name_data=$meta['company_name'][0];
          if($company_name==$company_name_data)
          {
          echo 'data-error="company_name"';
          $bug=1;
          }
        }
    }
    /*kiểm tra phone number*/
     foreach($users_all as $user)
    {
     if($user->ID!=$current_user_id)
        {
          $meta=get_user_meta( $user->ID );
          $phone_number_data=$meta['phone_number'][0];
          if($phone_number==$phone_number_data)
          {
          echo 'data-error="phone_number"';
          $bug=1;
          }
        }
    }
    /*kiểm tra email*/
       foreach($users_all as $user)
    {
        if($user->ID!=$current_user_id)
        {
          $user_email_data=$user->user_email;
            if($user_email==$user_email_data)
          {
          echo 'data-error="user_email"';
          $bug=1;
          }
      }
    }
    /*update dữ liệu*/
    if($bug==0)
    {
    if($user_email!=""){ update_post_meta( $company_id, '_company_email', $user_email ); }
    if($company_name!=""){ 
      $my_post = array(
    'ID'           => $company_id,
    'post_title'   => $company_name,
    'post_author'  => $current_user_id,
);
wp_update_post( $my_post );
    }
    if($phone_number!=""){ update_post_meta( $company_id, '_company_phone', $phone_number );  }
    if($city_name!=""){ update_user_meta($user_id,'city_name',$city_name);  
    $term_id[]=get_term_by('slug', $term_name , 'company_location_city')->term_id;
    wp_set_post_terms( $company_id, $term_id , 'company_location_city' );
    }
    if($district_name!=""){ update_post_meta( $company_id, '_company_location', $district_name ); }
    if($logo_id!=""){ set_post_thumbnail( $company_id, $logo_id ); }
    echo'<div id="error-form" class="error-form success" ><div class="warp-error"><div class="button-error-close" onclick="close_error()">×</div><div class="img-error"></div><p class="error-text">Update dữ liệu thành công</p></div></div>';
    }
    $result = ob_get_clean();
    wp_send_json_success($result); 
    die();
}
add_action( 'wp_ajax_change_company_profile', 'change_company_profile' );
add_action( 'wp_ajax_nopriv_change_company_profile', 'change_company_profile' );

function check_conditions()
{
  if( is_user_logged_in() )
  { ?>
      <script type="text/javascript">
        jQuery(function($){
          var id_last_page="$_SESSION['id_last_page']";
          if(id_last_page==239){
          window.location.href="<?= get_home_url(); ?>/book-mark";
                }
            else{
            window.location.href="<?= get_home_url(); ?>/job-dashboard";
            }
        })
      </script>
  <?php }
  else{ ?>
    <script type="text/javascript">
        jQuery(function($){
          var err=$('button#button-iquSqc');
          function coppy_paste()
          {
            var html_new=$('.erf-checkbox-group.form-group.erf-element-width-1').find('.parsley-errors-list').clone();
            $('.erf-richtext.erf-element-width-11').after(html_new);
            $('.erf-checkbox-group.form-group.erf-element-width-1').find('.parsley-errors-list').remove();
          }
          function coppyerr()
          {
            $('button#button-iquSqc').click(function(){
            var box=$('.erf-checkbox-group.form-group.erf-element-width-1').find('.parsley-errors-list');
          if(!box){ setTimeout( coppy_paste , 100 );} else { setTimeout( coppy_paste , 0 ); }
          })
          }
      if(!err){ setTimeout( coppyerr , 100 ) } else { setTimeout( coppyerr , 0 ) }
        })
      </script>
  <?php }
}
add_shortcode('check_conditions','check_conditions');


/*string to slug*/
function createSlug($str, $delimiter = '-'){

    $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
    return $slug;

} 
/*auto update company and jobs*/
 function auto_draft_company()
 {
          $args_company = array(
          'post_type'=>'company',
          'post_status'=>'any',
          'nopaging' => true,);
          $company_query = new WP_Query( $args_company );
          if ( $company_query->have_posts() ) 
          {
            $companies=$company_query->posts;
          } 
          wp_reset_query();
          //
          $args_jobs = array(
          'post_type'=>'job_listing',
          'post_status'=> array('any','expired'),
          'nopaging' => true,);
          $job_query = new WP_Query( $args_jobs );
          if ( $job_query->have_posts() ) 
          {
            $jobs=$job_query->posts;
          } 
          wp_reset_query();
          //
          foreach ($jobs as $value => $job) 
            {
              $time_expires_job=get_post_meta( $job->ID, '_job_expires', true );
              if(time() - strtotime($time_expires_job)>=0 && $job->post_status!="expired")
              {
                 $my_post = array(
                 'ID'           => $job->ID,
                 'post_status'   => 'expired',
                 'post_author'  => $job->post_author,
                 );
                 wp_update_post( $my_post );
              }
              elseif(time() - strtotime($time_expires_job)<0 && $job->post_status=="expired")
              {
              	$my_post = array(
                 'ID'           => $job->ID,
                 'post_status'   => 'publish',
                 'post_author'  => $job->post_author,
                 );
                 wp_update_post( $my_post );
              }
            }
          //
          foreach ($companies as $value => $company) 
          {
            $company_name=$company->post_title;
            $have_jobs=0;
            if(get_post_meta( $company->ID, '_company_location', true )=="")
            {
              $location="";
              $location_name=array();
              $location_name[]=wp_get_post_terms( $company->ID, 'company_location_city', array( 'fields' => 'names' ) );
              foreach ($location_name[0] as $value =>$name ) {
                    $location.=$name.", ";
                 }
                 $location=substr($location, 0, -2);
                 update_post_meta( $company->ID, '_company_location', $location );
            }
            if(get_the_company_job_listing_count( $company )==0 && $company->post_status!='no_jobs')
            {
                 $my_post = array(
                 'ID'           => $company->ID,
                 'post_status'   => 'no_jobs',
                 'post_author'  => $company->post_author,
                 );
                 wp_update_post( $my_post );
            }
            elseif(get_the_company_job_listing_count( $company )>0 && $company->post_status!='publish')
            {
                 $my_post = array(
                 'ID'           => $company->ID,
                 'post_status'   => 'publish',
                 'post_author'  => $company->post_author,
                 );
                 wp_update_post( $my_post );
            }
          }
          //
          $args_jobs_2 = array(
          'post_type'=>'job_listing',
          'post_status'=> array('any','expired'),
          'nopaging' => true,);
          $job_query_2 = new WP_Query( $args_jobs_2 );
          if ( $job_query_2->have_posts() ) 
          {
            $jobs_2=$job_query_2->posts;
          } 
          wp_reset_query();
          //
          foreach ($jobs_2 as $value => $job_2) 
            {
             $author_id= $job_2->post_author ;
             $company_mata= get_user_meta($author_id) ;
             $company_name=$company_mata['company_name'][0];
             $company_id=$company_mata['id_company'][0];
             $company_email=get_user_by( 'ID', $author_id )->user_email;

             if($company_id != get_post_meta( $job_2->ID, '_company_name', true ))
              {
                 update_post_meta( $job_2->ID, '_company_name', $company_id );
              }
              if($company_email!= get_post_meta( $job_2->ID, '_application', true ) )
              {
                 update_post_meta( $job_2->ID, '_application', $company_email );
              }
            }
          //
          $blogusers = get_users( array( 'role' => array( 'employer' ) ) );
foreach ( $blogusers as $user ) {
$company_meta=get_user_meta($user->ID);
$company_id=$company_meta['id_company'][0];
$company_name=$company_meta['company_name'][0];
$company_thumbnail_id= get_post_thumbnail_id($company_id);
$args = array(
'post_type'=>'job_listing',
'author' => $user->ID,
'nopaging' => true);
$post_query = new WP_Query( $args );
            while ( $post_query->have_posts() ) 
            { 
              $post_query->the_post();
              $post_id=get_the_ID();
              if(get_post_thumbnail_id($post_id)=="" || get_post_thumbnail_id($post_id)!=$company_thumbnail_id){
              set_post_thumbnail( $post_id, $company_thumbnail_id );
              }
              if(get_post_meta( $post_id, '_job_location', true )=="")
              {
              $location="";
              $location_name=array();
              $location_name[]=wp_get_post_terms( $post_id, 'job_location_city', array( 'fields' => 'names' ) );
              foreach ($location_name[0] as $value =>$name ) {
                    $location.=$name.", ";
                 }
                 $location=substr($location, 0, -2);
                 update_post_meta( $post_id, '_job_location', $location );
              }
            }
wp_reset_query();
}

}
add_action('admin_init','auto_draft_company');

/*company no jobs*/
function wpdocs_custom_post_status(){
    register_post_status( 'no_jobs', array(
        'label'                     => _x( 'No jobs', 'company' ),
        'public'                    => false,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'No jobs <span class="count">(%s)</span>', 'No jobs <span class="count">(%s)</span>' ),
    ) );
}
add_action( 'init', 'wpdocs_custom_post_status' );

/*tắt cập nhật*/
add_filter( 'auto_update_plugin', '__return_false' );
add_filter( 'auto_update_theme', '__return_false' );
add_filter('pre_site_transient_update_core','remove_core_updates');

/*** Sort and Filter Users ***/
function new_modify_user_table( $column ) {
    $column['id_company'] = 'Company ID';
    return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table' );
//
function new_modify_user_table_short( $column ) {
    $column['id_company'] = 'id_company';
    return $column;
}
add_filter( 'manage_users_sortable_columns', 'new_modify_user_table_short');
function smashing_users_orderby( $query ) {
  if ( 'id_company' === $query->get( 'orderby') ) {
    $query->set( 'orderby', 'meta_value' );
    $query->set( 'meta_key', 'id_company' );
    $query->set( 'meta_type', 'numeric' );
  }
}
add_action( 'pre_get_users', 'smashing_users_orderby' );
//
function new_modify_user_table_row( $val, $column_name, $user_id ) {
    switch ($column_name) {
        case 'id_company' :
            return '<a href="'.get_home_url().'/wp-admin/post.php?post='.get_the_author_meta( 'id_company', $user_id ).'&action=edit">'.get_the_author_meta( 'id_company', $user_id ).'</a>';
        default:
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );
/**/
function new_modify_user_table_2( $column ) {
    $column['company_name'] = 'Company name';
    return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table_2' );
//
function new_modify_user_table_short_2( $column ) {
    $column['company_name'] = 'company_name';
    return $column;
}
add_filter( 'manage_users_sortable_columns', 'new_modify_user_table_short_2');
function smashing_users_orderby_2( $query ) {
  if ( 'company_name' === $query->get( 'orderby') ) {
    $query->set( 'orderby', 'meta_value' );
    $query->set( 'meta_key', 'company_name' );
  }
}
add_action( 'pre_get_users', 'smashing_users_orderby_2' );
//
function new_modify_user_table_row_2( $val, $column_name, $user_id ) {
    switch ($column_name) {
        case 'company_name' :
            return '<a href="'.get_home_url().'/wp-admin/post.php?post='.get_the_author_meta( 'id_company', $user_id ).'&action=edit">'.get_the_author_meta( 'company_name', $user_id ).'</a>';
        default:
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row_2', 10, 4 );

/*post author company*/
function new_modify_company_table( $column ) {
    $column['posted_by'] = 'Posted by';
    return $column;
}
add_filter( 'manage_company_posts_columns', 'new_modify_company_table' );
//
function new_modify_company_table_short( $column ) {
    $column['posted_by'] = 'posted_by';
    return $column;
}
add_filter( 'manage_edit-company_sortable_columns', 'new_modify_company_table_short');
function mycpt_custom_orderby( $query ) {
  if ( 'posted_by' === $query->get( 'orderby') ) {
    $query->set( 'orderby', 'meta_value' );
    $query->set( 'meta_key', '_company_email' );
  }
}
add_action( 'pre_get_posts', 'mycpt_custom_orderby' );
//
function new_modify_company_table_row(  $column_name, $post_ID ) {
    switch ($column_name) {
        case 'posted_by' :
            $author_id = get_post_field ('post_author', $post_ID);
            $display_name = get_the_author_meta( 'display_name' , $author_id ); 
            echo '<a href="'.get_home_url().'/wp-admin/user-edit.php?user_id='.$author_id.'">'.$display_name.'</a>';
        default:
    }
    return $val;
}
add_filter( 'manage_company_posts_custom_column', 'new_modify_company_table_row', 10, 3 );
//
function views_table_short( $column ) {
    $column['views'] = 'views';
    return $column;
}
add_filter( 'manage_edit-company_sortable_columns', 'views_table_short');
add_filter( 'manage_edit-job_listing_sortable_columns', 'views_table_short');
function views_custom_orderby( $query ) {
  if ( 'posted_by' === $query->get( 'orderby') ) {
    $query->set( 'orderby', 'meta_value' );
    $query->set( 'meta_key', 'views' );
    $query->set( 'meta_type', 'numeric' );
  }
}
add_action( 'pre_get_posts', 'views_custom_orderby' );
//
function new_modify_jobs_table_row(  $column_name, $post_ID ) {
    switch ($column_name) {
        case 'job_position' :
            
            echo '<a href="'.get_home_url().'/wp-admin/post.php?post='.get_post_meta( $post_ID, '_company_name', true ).'&action=edit">'.get_post( get_post_meta( $post_ID, '_company_name', true ) )->post_title.'</a>';
        default:
    }
    return $val;
}
add_filter( 'manage_job_listing_posts_custom_column', 'new_modify_jobs_table_row', 10, 3 );

function check_verify_end()
{
  if( is_user_logged_in() )
  { ?>
      <script type="text/javascript">
        jQuery(function($){
          var id_last_page="$_SESSION['id_last_page']";
          if(id_last_page==239){
          window.location.href="<?= get_home_url(); ?>/book-mark";
                }
            else{
            window.location.href="<?= get_home_url(); ?>/job-dashboard";
            }
        })
      </script>
  <?php }
  else{ ?>
    <script type="text/javascript">
        jQuery(function($){
          if($('h1.site-content-page-title').text()=="For recruiter")
           {
             $('.messager-text-info-candidate').css('display','none');
           }
           else
           {
             $('.messager-text-info-employer').css('display','none');
           }
        })
      </script>
  <?php }
}
add_shortcode('check_verify_end','check_verify_end');

/*send email admin*/
function send_admin_email($post_id, $post, $update) 
{
  if($post->post_type=='job_listing' && $post->post_status=='pending')
  {
    $count = count(get_posts(array('post_type' => 'job_listing','post_status' => 'pending','nopaging' => true)));
    $job_name=array();
    $jobs_company=array();
    foreach (get_posts(array('post_type' => 'job_listing','post_status' => 'pending','nopaging' => true)) as $value => $job) {
    $job_name[]=$job->post_title;
    $jobs_company[]= get_user_meta($job->post_author)['company_name'][0];
  }
    $job_out="";
    for($i=0;$i<count($job_name);$i++) 
    {
      $job_out.='<br>'.$jobs_company[$i].' ----- '.$job_name[$i];
    }
    if($count>1)
    {
      $to = get_option('admin_email');
      $subject = 'Job approval';
      $message = $count.' jobs are in queue for review and approval.'.$job_out;
      wp_mail($to, $subject, $message );
    }
    elseif($count==1)
    {
      $to = get_option('admin_email');
      $subject = 'Job approval';
      $message = '1 job are in queue for review and approval.'.$job_out;
      wp_mail($to, $subject, $message );
    }
  }
}
add_action( 'wp_insert_post', 'send_admin_email', 10, 3 );
add_filter('wp_mail_content_type', function( $content_type ) {
            return 'text/html';
});
