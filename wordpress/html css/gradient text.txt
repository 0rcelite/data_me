section.jh-section.banners-block.banner-2 .section-title {
    line-height: 40px;
    font-size: 40px;
    background: linear-gradient(
90deg
, #FFF511 1.57%, #FFF511 52.84%, #FFFFFF 100%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
	 color:#FFF511;
    text-transform: capitalize;
	filter: drop-shadow(1px 5px 3px #0000004D);
}