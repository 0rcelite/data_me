.line-115 a
{
    color: #000;
    font-family: "Cormorant Garamond";
    font-size: 45px;
    font-weight: 700;
    text-transform: capitalize;
    line-height: 50px;
}
@media screen and (min-width :768px) and (max-width :1209px)
{
    .line-115 a
{
    font-size: 25px;
    line-height: 30px;
}
}
@media screen and (max-width :767px)
{
       .line-115 a
{
    font-size: 20px;
    line-height: 25px;
} 
}