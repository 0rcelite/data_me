flex-direction:
row: Chuyển trục main axis thành chiều ngang, nghĩa là hiển thị theo hàng.
colum: Chuyển trục main axis thành chiều dọc, nghĩa là hiển thị theo cột.
row-reverse: Hiển thị theo hàng nhưng đảo ngược vị trí các item.
column-reverse: Hiển thị theo cột nhưng đảo ngược vị trí các item.