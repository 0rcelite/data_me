var swiper = new Swiper(".mySwiper", {
		autoplay: {
        delay: 6000,
        },
		navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
        },
        slidesPerView: 2,
        centeredSlides: true,
        spaceBetween: 30,
        grabCursor: true,
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
      });



.home-product .swiper {
        width: 100%;
        height: 100%;
      }

      .swiper-slide {
        text-align: center;
        font-size: 18px;
        height:100%;

        /* Center slide text vertically */
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
      }
.home-product .swiper-slide.swiper-slide-active img {
	opacity:1;
}
     .swiper-slide img {
	opacity:0.5;
    display: block;
    width: 60%;
    padding: 0 0 100px;
    object-fit: cover;
}
.home-product .swiper-button-next,.home-product .swiper-container-rtl .swiper-button-prev,
.home-product .swiper-button-prev,.home-product .swiper-container-rtl .swiper-button-next{
	color:#fff;
	display:flex;
	justify-content:center;
	align-items:center;
    border: 1px solid #E0E0E0;
    width: 50px;
    height: 50px;
    background-image: unset;
    border-radius: 50%;
}
.home-product .swiper-button-next,.home-product .swiper-container-rtl .swiper-button-prev{
    right: 15%;
    left: auto;
}
.home-product .swiper-button-prev,.home-product .swiper-container-rtl .swiper-button-next{
    left: 15%;
    right: auto;
}
.home-product span.swiper-pagination-bullet {
    width: 30px;
    height: 3px;
    border-radius: 5px;
	background-color:#E0E0E0;
	opacity:1;
}
.home-product span.swiper-pagination-bullet.swiper-pagination-bullet-active
{
  position:relative;
  width:60px;	
}
.home-product span.swiper-pagination-bullet.swiper-pagination-bullet-active:after
{
  z-index: 2;
    left: 0;
    content: "";
    width: 100%;
    position: absolute;
    height: 100%;
    animation: 6000ms linear 1 Cooldown;
	background-color:#EE0033;
}



<div class="home-product">
      <div class="swiper mySwiper">
	  <div class="swiper-button-prev"><i class="fas fa-chevron-left"></i></div>
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          <img src="/wp-content/uploads/2021/10/HC1.png" />
        </div>
        <div class="swiper-slide">
          <img src="/wp-content/uploads/2021/10/HC2.png" />
        </div>
        <div class="swiper-slide">
          <img src="/wp-content/uploads/2021/10/HC3.png" />
        </div>
      </div>
	  <div class="swiper-button-next"><i class="fas fa-chevron-right"></i></div>
      <div class="swiper-pagination"></div>
</div>
    </div>