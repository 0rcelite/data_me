<script type="text/javascript">
    jQuery(function($){
    if($(window).width()>767){
  var swiper = new Swiper('.swiper-container', {
 slidesPerView: 1,
  spaceBetween: 0,
  direction:    'horizontal',
  initialSlide: 0,
  PreventInteractionOnTransition: true,
  navigation: {
    nextEl: '.slick-next',
    prevEl: '.slick-prev',
  },
  keyboard: {
    enabled: true,
    onlyInViewport: false,
  }
   });}
  else if($(window).width()<768)
  {
    var swiper = new Swiper('.swiper-container', {
 slidesPerView: "auto",
        spaceBetween: 0,
  navigation: {
    nextEl: '.slick-next',
    prevEl: '.slick-prev',
  },
  keyboard: {
    enabled: true,
    onlyInViewport: false,
  }
   });
  }
    
    })
    </script>