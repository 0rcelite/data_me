add_action('init', 'myStartSession', 1);

function myStartSession() { 

     if(!session_id()) {

        session_start();
    }
}
add_action('wp_head','myStartSession_jquery');
function myStartSession_jquery() { ?>

	<script type="text/javascript">jQuery(function($){
         function ajax_width(){
                $.ajax({
                    type : "post", 
                    dataType : "json", 
                    url : '<?php echo admin_url('admin-ajax.php');?>', 
                    data : {
                        action: "ajax_width",
                        window : $(window).width(),
                    },
                    context: this,
                    beforeSend: function(){
                    },
                    success: function(response) {
                    	 if(response.data=='ok'){  location.reload(); }
                    },
                    error: function(){
                    }
                })
        }
        ajax_width();
  })
    </script>  
 <?php  } 
/*function ajax cookie*/
function ajax_width() {
    ob_start();
    $width =  (isset($_POST['window']))?esc_attr($_POST['window']) : 0;
    if(!isset($_SESSION['width']))
   {
   	$_SESSION['width']=$width;
   }
   else
   { 
     if($_SESSION['width']!=$width)
     {
     	$_SESSION['width']=$width;
        echo 'ok';
     }
     else{}
   }
    $result = ob_get_clean();
    wp_send_json_success($result); 
    die();
}
add_action( 'wp_ajax_ajax_width', 'ajax_width' );
add_action( 'wp_ajax_nopriv_ajax_width', 'ajax_width' );
add_action('wp_logout', 'myEndSession');
add_action('wp_login', 'myEndSession');
function myEndSession() {
    session_destroy ();
}