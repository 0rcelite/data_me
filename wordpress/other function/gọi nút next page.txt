﻿<?php if(paginate_links()!='') {?>
	<div class="quatrang">
		<?php
		global $wp_query;
		$big = 999999999;
		echo paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'prev_text'    => __('« Mới hơn'),
			'next_text'    => __('Tiếp theo »'),
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages
			) );
	    ?>
	</div>
<?php } ?>

global $wp_query;
        $total   = isset( $total ) ? $total : jh_wpjmc_get_loop_prop( 'total_pages' );
        $current = isset( $current ) ? $current : jh_wpjmc_get_loop_prop( 'current_page' );
        $base    = isset( $base ) ? $base : esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) );
        $format  = isset( $format ) ? $format : '';
        if ( $total <= 1 ) {
            return;
        }
        ?>
        <nav class="wpjmc-pagination">
            <?php
                echo paginate_links( apply_filters( 'jh_wpjmc_pagination_args', array( // WPCS: XSS ok.
                    'base'         => $base,
                    'format'       => $format,
                    'add_args'     => false,
                    'current'      => max( 1, $current ),
                    'total'        => $total,
                    'prev_text'    => is_rtl() ? '&rarr;' : '&larr;',
                    'next_text'    => is_rtl() ? '&larr;' : '&rarr;',
                    'type'         => 'list',
                    'end_size'     => 3,
                    'mid_size'     => 3,
                ) ) );
            ?>
        </nav>