﻿if ( ! function_exists( 'home_projects' ) ) {
function home_projects($args,$content) {
	?>
     <?php if(!isset($args['cat_id'])){$cat_id=102;} else {$cat_id=$args['cat_id'];}?>
    <?php if(!isset($args['numb_show'])){$numb=4;} else {$numb=$args['numb_show'];}?>
	<div class="wrap_ajax_projects">
	<div class="ajax_projects">
    </div>
    <div class="prev"><i class="fas fa-chevron-left"></i></div>
    <div class="next"><i class="fas fa-chevron-right"></i></div>
    </div>
	<script type="text/javascript">jQuery(function($){
        var cat_id=<?php echo $cat_id ?>;
        var numb=<?php echo $numb ?>;
        var index;
        function ajax_home_projects(n){
        	if(n<1){index=1;}
        	else if(n>3){index=3}
        	else {index=n} 

        	if(index==1){$('.prev').addClass('end');}
            else{$('.prev').removeClass('end');}
            if(index==3){$('.next').addClass('end');}
            else{$('.next').removeClass('end');}
                $.ajax({
                    type : "post", 
                    dataType : "json", 
                    url : '<?php echo admin_url('admin-ajax.php');?>', 
                    data : {
                        action: "windows", 
                        windows : $(window).width(),
                        cat_id : cat_id,
                        page: index,
                        numb: numb,
                    },
                    context: this,
                    beforeSend: function(){
                    },
                    success: function(response) {
                        if(response.success) {
                            $('.ajax_projects').html(response.data);
                        }
                        else {
                            alert('Đã có lỗi xảy ra');
                        }
                    },
                    error: function(){
                    }
                })
        }
    ajax_home_projects(1);
     function ajax_home_projects_plus(n){
     	ajax_home_projects(index+=n);
     }
    $('.next').click(function(){
    	 ajax_home_projects_plus(1);
    });
    $('.prev').click(function(){
    	 ajax_home_projects_plus(-1);
    });
    })
</script>
	<?php
}
}