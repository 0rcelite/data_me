﻿<?php

 if ( ! function_exists( 'home_projects' ) ) {
function home_projects($args,$content) {
	$deafault_lang=get_option('polylang');
    $current_lang=pll_current_language();
    $deafault_lang['default_lang']=$current_lang;
    update_option('polylang',$deafault_lang);
    ?>
     <?php if(!isset($args['cat_id'])){$cat_id=102;} else {$cat_id=$args['cat_id'];}?>
    <?php if(!isset($args['numb_show'])){$numb=12;} else {$numb=$args['numb_show'];}?>
    <?php if(!isset($args['numb_show_tablet'])){$numb_t=6;} else {$numb_t=$args['numb_show_tablet'];}?>
    <?php if(!isset($args['numb_show_mobile'])){$numb_m=3;} else {$numb_m=$args['numb_show_mobile'];}?>
	<div class="wrap_ajax_projects swiper-container-project swiper-container">
	<div class="ajax_projects swiper-wrapper">
    </div>
    </div>
    <div class="prev"><i class="fas fa-chevron-left"></i></div>
    <div class="next"><i class="fas fa-chevron-right"></i></div>
	<script type="text/javascript">jQuery(function($){
        function ajax_home_projects(){
        var cat_id=<?php echo $cat_id ?>;
        var numb;
        if($(window).width()>1209){numb=<?php echo $numb ?>;}
        else if($(window).width()<1210 && $(window).width()>767){numb=<?php echo $numb_t ?>;}
        else if($(window).width()<768){ numb=<?php echo $numb_m ?>; }
                $.ajax({
                    type : "post", 
                    dataType : "json", 
                    url : '<?php echo admin_url('admin-ajax.php');?>', 
                    data : {
                        action: "windows", 
                        windows : $(window).width(),
                        cat_id : cat_id,
                        numb: numb,
                    },
                    context: this,
                    beforeSend: function(){
                    },
                    success: function(response) {
                        if(response.success) {
    $('.ajax_projects').html(response.data);
   function initSwiper() 
{       if($(window).width()>767)
    {
        mySwiper = new Swiper('.swiper-container-project', {            
        spaceBetween: 0,
        direction: 'horizontal',
        navigation: 
        {
           nextEl: '.next',
           prevEl: '.prev',
        }                                                  }); 
    } 
}
initSwiper();

                        }
                        else {
                            alert('Đã có lỗi xảy ra');
                        }
                    },
                    error: function(){
                    }
                })
        }
    ajax_home_projects();
    $(window).on('resize', function(){
     ajax_home_projects();       
});
    })
</script>
	<?php
}
add_shortcode( 'home_projects', 'home_projects' );
}

 if ( ! function_exists( 'windows' ) ) {
function windows() {
	ob_start();
	$window_width = (isset($_POST['windows']))?esc_attr($_POST['windows']) : '';
	$cat_id = (isset($_POST['cat_id']))?esc_attr($_POST['cat_id']) : '';
    $post_query=null;
    $numb =  (isset($_POST['numb']))?esc_attr($_POST['numb']) : '';
    $args = array(
'post_type'=>'post',
'post_status'=>'publish',
'cat' => $cat_id,
'posts_per_page' => $numb,
'orderby' => 'publish_date',
'order' => 'DESC');
$post_query = new WP_Query( $args );
$var_i=1;
      if($post_query->have_posts()):

            while ( $post_query->have_posts() ) { ?>
						
						<?php

						$post_query->the_post();

							$post_id=get_the_ID();

                        if($window_width>1209){
                                 if($var_i==1 || $var_i%4==1)
                                 {
                                    echo '<div class="wrap-grid swiper-slide">';
                                 }
                            ?>
						<div class="wrap-post swiper-slide-item">
									<div class="elementor-post__thumbnail"><img class="img-post" src="<?=get_the_post_thumbnail_url( esc_attr( $post_id ), 'large' ) ?>"/>
									</div>
						    <div class="elementor-post__text">
						    <h3 class="elementor-post__title"><?=get_the_title( esc_attr( $post_id ) ) ?>
						    </h3>
                            </div>
                            <div class="proudprojects-mask">
                            	<div class="wrap-mask">
									<h3><?php the_title(); ?></h3>
									<span><?php the_field('project_type'); ?></span>
									<p><?php the_field('project_description'); ?></p>
                                    <a /*href="<?= esc_url( get_the_permalink($post_id))?>"*/><span><?= pll__('View detail') ?></span></a>
								</div>
							</div>
                        </div>
<?php
    
     if($var_i==4 || $var_i%4==0 )
                                 {
                                    echo '</div>';
                                 }
        $var_i++;

    }
      else if($window_width>767 && $window_width<1210)
    { 
         if($var_i==1 || $var_i%2==1)
                                 {
                                    echo '<div class="wrap-grid swiper-slide">';
                                 }
        ?>
                        <?php echo '<a class="wrap-post" /*href="'.esc_url( get_the_permalink($post_id)).'"*/>';?>
                                    <div class="elementor-post__thumbnail"><img class="img-post" src="<?=get_the_post_thumbnail_url( esc_attr( $post_id ), 'large' ) ?>"/>
                                    </div>
                            <div class="elementor-post__text">
                            <h3 class="elementor-post__title"><?=get_the_title( esc_attr( $post_id ) ) ?>
                            </h3>
                            </div>
                        <?php echo '</a>';?>
    <?php 
     if($var_i==2 || $var_i%2==0 )
                                 {
                                    echo '</div>';
                                 }
        $var_i++;
    }
    else
    { ?>
                        <?php echo '<a class="wrap-post" /*href="'.esc_url( get_the_permalink($post_id)).'"*/>';?>
                                    <div class="elementor-post__thumbnail"><img class="img-post" src="<?=get_the_post_thumbnail_url( esc_attr( $post_id ), 'large' ) ?>"/>
                                    </div>
                            <div class="elementor-post__text">
                            <h3 class="elementor-post__title"><?=get_the_title( esc_attr( $post_id ) ) ?>
                            </h3>
                            </div>
                        <?php echo '</a>';?>
    <?php 
    }
}
endif; wp_reset_query();?>

  <?php
    $result = ob_get_clean();
    wp_send_json_success($result); 
    die();
}
add_action( 'wp_ajax_windows', 'windows' );
add_action( 'wp_ajax_nopriv_windows', 'windows' );
}
