function edit_job_new()
{
$post_id = $_GET['id'];
?>
       <form id="new_post" class="wrap-form login info space-between post-job " method="post" action="" >
        <h1 class="title-form-contain w100">Edit Job</h1>
        <div class="form-group-job-title w100">
            <label for="job_title">Job Title</label>
            <input type="text" name="job_title" class="form-control" placeholder="Tiêu đề" value="<?= get_the_title( $post_id ) ?>">
        </div>
        <div class="form-group-job-description w100">
          <label for="job_content">Job Description </label>
          <?php wp_editor( get_post($post_id)->post_content, 'job-description', array( 'textarea_name' => 'job_content' ));?>
        </div>
        <div class="form-group-job-requirement w100">
          <label for="job_requirement">Job Requirement </label>
          <?php wp_editor( get_post_meta( $post_id, 'job_requirement', true ) , 'job_requirement', array( 'textarea_name' => 'job_requirement' ));?>
        </div>
        <div class="form-group-job-type w50">
          <label for="job_listing_type">Job Type</label>
         <?php drop_taxonomy2('job_listing_type','-- select job type --', $post_id); ?>       
        </div>

         <div class="form-group-job-category w50 more-option">
          <label for="job_listing_category">Category</label>
          <ul class="ul_job_listing_category">
        <?php $val_job_listing_category=wp_get_post_terms( $post_id, 'job_listing_category', array( 'fields' => 'names' ) ); $val_lo_input="";
        foreach ($val_job_listing_category as $value => $val): $val_lo_input.=$val.', ';
        ?>
        <li class="li_job_listing_category" title="<?= $val ?>"><p class="text-val"><?= $val ?></p><p class="li-value-delete">×</p></li>
        <?php endforeach; ?>
        <li class="li_job_listing_category_last"><input class="li-input-text-show_job_listing_category" type="text" data-for="job_listing_category"></li>
        </ul>
          <input type="hidden" name="job_listing_category_input" class="job-listing-category" value="<?= $val_lo_input ?>" />
          <div class="job_listing_category_autocomplete" style="display: none;">
            <?php $terms=get_terms(['taxonomy' => 'job_listing_category','hide_empty' => false,]); 
            foreach ( $terms as $terms_name) : ?>
              <div class="autocomplete_text" style="display: none"><?=$terms_name->name?></div>
            <?php endforeach;?>
              <div class="autocomplete_text not_found" style="display: none">No Category found</div> 
          </div>  
        </div>

        <div class="form-group-job-career-level w50">
          <label for="job_listing_career_level">Level</label>
         <?php drop_taxonomy2('job_listing_career_level','-- select job career level --', $post_id); ?>       
        </div>

        <div class="form-group-job-industry w50 more-option">
          <label for="job_listing_industry">Industry</label>
        <ul class="ul_job_listing_industry">
        <?php $val_job_listing_industry=wp_get_post_terms( $post_id, 'job_listing_industry', array( 'fields' => 'names' ) ); $val_lo_input="";
        foreach ($val_job_listing_industry as $value => $val): $val_lo_input.=$val.', ';
        ?>
        <li class="li_job_listing_industry" title="<?= $val ?>"><p class="text-val"><?= $val ?></p><p class="li-value-delete">×</p></li>
        <?php endforeach; ?>
        <li class="li_job_listing_industry_last"><input class="li-input-text-show_job_listing_industry" type="text" data-for="job_listing_industry"></li>
        </ul>
          <input type="hidden" name="job_listing_industry_input" class="job-listing-industry" value="<?= $val_lo_input ?>" />
          <div class="job_listing_industry_autocomplete" style="display: none;">
            <?php $terms=get_terms(['taxonomy' => 'job_listing_industry','hide_empty' => false,]); 
            foreach ( $terms as $terms_name) : ?>
              <div class="autocomplete_text" style="display: none"><?=$terms_name->name?></div>
            <?php endforeach;?>
              <div class="autocomplete_text not_found" style="display: none">No Industry found</div> 
          </div>     
        </div>

        <div class="form-group-job-experience w50">
          <label for="job_listing_experience">Experience</label>
         <?php drop_taxonomy2('job_listing_experience','-- select job experience --', $post_id); ?>       
        </div>
        <div class="form-group-job-qualification w50">
          <label for="job_listing_qualification">Qualification</label>
         <?php drop_taxonomy2('job_listing_qualification','-- select job qualification --', $post_id); ?>       
        </div>

        <div class="form-group-job-location-city w100 more-option">
          <label for="job_location_city">Work Location</label>
        <ul class="ul_job_location_city">
        <?php $val_job_location_city=wp_get_post_terms( $post_id, 'job_location_city', array( 'fields' => 'names' ) ); $val_lo_input="";
        foreach ($val_job_location_city as $value => $val): $val_lo_input.=$val.', ';
        ?>
        <li class="li_job_location_city" title="<?= $val ?>"><p class="text-val"><?= $val ?></p><p class="li-value-delete">×</p></li>
        <?php endforeach; ?>
        <li class="li_job_location_city_last"><input class="li-input-text-show_job_location_city" type="text" data-for="job_location_city"></li>
        </ul>
          <input type="hidden" name="job_location_city_input" class="job-location-city" value="<?= $val_lo_input ?>" />
          <div class="job_location_city_autocomplete" style="display: none;">
            <?php $terms=get_terms(['taxonomy' => 'job_location_city','hide_empty' => false,]); 
            foreach ( $terms as $terms_name) : ?>
              <div class="autocomplete_text" style="display: none"><?=$terms_name->name?></div>
            <?php endforeach;?>
              <div class="autocomplete_text not_found" style="display: none">No Work Location found</div>
          </div> 
        </div>

        <div class="form-group-job-skill-requirement w100">
          <label for="skill_requirement">Skill requirement</label>
          <input type="text" name="skill_requirement" class="job-skill-requirement" value="<?= get_post_meta( $post_id, 'skill_requirement',  true ) ?>" />
        </div>
       <div class="form-group-job-expires-datepicker w100">
          <label for="_job_expires-datepicker">Application deadline</label>
         <select class="search_category" name="_job_expires-datepicker" style="color:#555">
            <option value="7d" style="color:#555" selected>Expired in 7 days</option>
            <option value="15d" style="color:#555">Expired in 15 days</option>
            <option value="30d" style="color:#555">Expired in 30 days</option>
         </select>      
        </div>
        <input type="hidden" name="update_post" value="<?= $post_id ?>" />
        <?php wp_nonce_field( 'post_nonce', 'post_nonce_field' ); ?>
        <div class="form-group">
            <div class="col-sm-12" style="padding-left:0;">
              <button type="submit" class="btn btn-primary save">Save</button><a class="btn btn-primary cancel" onclick='window.location.href ="<?= home_url( '/job-dashboard/' ); ?>"'>Cancel</a>
            </div>
        </div>
    </form>
    <script type="text/javascript">jQuery(function($){
      function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
}
      function out_click(name)
        {
          $(document).on('click', function (e) 
          {
            if ($(e.target).closest('.'+name+'_autocomplete').length === 0) 
            {
              $('.'+name+'_autocomplete').hide();
            }
          });
        }
        out_click('job_location_city');
        out_click('job_listing_industry');
        out_click('job_listing_category');
      function focus(name)
        {
          $('ul.ul_'+name).click(function(){ $('input.li-input-text-show_'+name).focus(); });
        }
        focus('job_location_city');
        focus('job_listing_industry');
        focus('job_listing_category');
      function search(name)
        {  
          $('input.li-input-text-show_'+name).on("change paste keyup", function() 
          {
            if($(this).val()!="")
              {
                var text=xoa_dau($(this).val());
                count_found=0;
                text_input_show=new RegExp('^'+text+'.*', 'i');
                $('.'+name+'_autocomplete .autocomplete_text').each(function()
                  {  
                    var text_option=$(this).text();
                    if( text_input_show.test(text_option) )
                      { 
                        $(this).css('display','block');
                        $('.'+name+'_autocomplete').css('display','block');
                        count_found++;
                      }
                    else 
                      {
                        $(this).css('display','none');
                      }
                  });
                if(count_found==0)
                  { 
                    $('.'+name+'_autocomplete .autocomplete_text.not_found').css('display','block'); 
                    $('.'+name+'_autocomplete').css('display','block');
                  }
              }
            else
              {
                $('.'+name+'_autocomplete').css('display','none');
              }
          });
        search_click(name);
        }
        search('job_location_city');
        search('job_listing_industry');
        search('job_listing_category');
        function search_click(name)
        {
          $('div.more-option .'+name+'_autocomplete .autocomplete_text').click(function()
            {
              var input_value=$('input[name="'+name+'_input"]').val();
              if(input_value.indexOf($(this).text())== -1 )
                {
                  $('input[name="'+name+'_input"]').val(input_value+=$(this).text()+', ');
                  $('.li_'+name+'_last').before('<li class="li_'+name+'" title="'+$(this).text()+'"><p class="text-val">'+$(this).text()+'</p><p class="li-value-delete">×</p></li>');
                  $(this).css('display','none');
                  xoa_li(name);
                }
              else if(input_value.indexOf($(this).text())> -1 )
                {   
                  var val_dele=$(this).text()+', ';
                  var new_val=input_value.replaceAll(val_dele,"");
                  $('input[name="'+name+'_input"]').val(new_val);
                  $('li.li_'+name+'').each(function() {if( val_dele.indexOf($(this).text().replace("×",""))>-1) $(this).remove()});
                  $(this).css('display','none');
                }
                  $('.'+name+'_autocomplete').css('display','none');
                  $(this).parents('div.more-option').find('.li-input-text-show_'+name+'').val('');
                  $(this).parents('div.more-option').find('.li-input-text-show_'+name+'').focus();
          })
        }
        function xoa_li(name)
        {
            $('p.li-value-delete').click(function(e)
            { 
              var text_del=$(this).parents('li').text().replace("×","")+', '; 
              var new_val=$('input[name="'+name+'_input"]').val().replaceAll(text_del,"");
              $('input[name="'+name+'_input"]').val(new_val);
              $(this).parents('li').remove();
            });
        }
        xoa_li('job_location_city');
        xoa_li('job_listing_industry');
        xoa_li('job_listing_category');
})
    </script>
<?php 
if( $_SERVER['REQUEST_METHOD'] == 'POST' && !empty( $_POST['update_post'] ) && isset( $_POST['post_nonce_field'] ) && wp_verify_nonce( $_POST['post_nonce_field'], 'post_nonce' )) {

if (isset($_POST['job_title'])) {
    $job_title = $_POST['job_title'];
}
if (isset($_POST['job_content'])) {
    $job_content = $_POST['job_content'];
}
if (isset($_POST['job_requirement'])) {
    $job_requirement = $_POST['job_requirement'];
}
if (isset($_POST['job_listing_type'])) {
    $job_listing_type = $_POST['job_listing_type'];
}
if (isset($_POST['job_listing_category_input'])) {
    $job_listing_category[] = explode(", ",$_POST['job_listing_category_input']);
}
if (isset($_POST['job_listing_career_level'])) {
    $job_listing_career_level = $_POST['job_listing_career_level'];
}
if (isset($_POST['job_listing_industry_input'])) {
    $job_listing_industry[] = explode(", ",$_POST['job_listing_industry_input']);
}
if (isset($_POST['job_listing_experience'])) {
    $job_listing_experience = $_POST['job_listing_experience'];
}
if (isset($_POST['job_listing_qualification'])) {
    $job_listing_qualification = $_POST['job_listing_qualification'];
}
if (isset($_POST['job_location_city_input'])) {
    $job_location_city[] = explode(", ",$_POST['job_location_city_input']);
}
if (isset($_POST['skill_requirement'])) {
    $skill_requirement = $_POST['skill_requirement'];
}
if (isset($_POST['_job_expires-datepicker'])) {
  if($_POST['_job_expires-datepicker']=="7d")
  {
    $_job_expires = time()+ 604800;
  }
  elseif($_POST['_job_expires-datepicker']=="15d")
  {
    $_job_expires = time()+ 1296000;
  }
  else
  {
    $_job_expires = time()+ 2592000;
  }
}
if($_POST['job_title']!="")
{
$post_id = $_POST['update_post'];
//
$location=array();
foreach ($job_location_city[0] as $location_name) {
$location[]=get_term_by('name', $location_name, 'job_location_city')->term_id;
}
wp_set_post_terms( $post_id, $location , 'job_location_city' );
$cate=array();
foreach ($job_listing_category[0] as $cate_name) {
$cate[]=get_term_by('name', $cate_name, 'job_listing_category')->term_id;
}
wp_set_post_terms( $post_id, $cate , 'job_listing_category' );
$industry=array();
foreach ($job_listing_industry[0] as $industry_name) {
$industry[]=get_term_by('name', $industry_name, 'job_listing_industry')->term_id;
}
wp_set_post_terms( $post_id, $industry , 'job_listing_industry' );
//
update_post_meta( $post_id, 'job_requirement', $job_requirement );
update_post_meta( $post_id, '_job_expires', date("Y-m-d", $_job_expires) );
update_post_meta( $post_id, 'skill_requirement',  $skill_requirement );
//
wp_set_post_terms( $post_id, array(get_term_by('name', $job_listing_type, 'job_listing_type')->term_id), 'job_listing_type' );
wp_set_post_terms( $post_id, array(get_term_by('name', $job_listing_career_level, 'job_listing_career_level')->term_id), 'job_listing_career_level' );
wp_set_post_terms( $post_id, array(get_term_by('name', $job_listing_experience, 'job_listing_experience')->term_id), 'job_listing_experience' );
wp_set_post_terms( $post_id, array(get_term_by('name', $job_listing_qualification, 'job_listing_qualification')->term_id), 'job_listing_qualification' );
wp_update_post( array('ID'=> $post_id,'post_content' => $job_content,) );

?>
<script type="text/javascript">
  jQuery(function($){
     $('.wrap-form.login.info').after('<div id="error-form" class="error-form"><div class="warp-error"><div class="button-error-close">×</div><div class="img-error"></div><p class="error-text">Cập nhật job thành công</p></div></div>');
     $('.button-error-close').click(function(){ $('#error-form').remove(); window.location.href ="<?= $_SESSION['url_last_page_head'] ?>"; });
     $('#error-form').click(function(){ $(this).remove();window.location.href ="<?= $_SESSION['url_last_page_head'] ?>"; });
  })
</script>
<?php
}
}
}
add_shortcode( 'edit_job_new', 'edit_job_new' );