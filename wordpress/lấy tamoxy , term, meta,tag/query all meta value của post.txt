/*xóa tag từ text php*/
function strip_tags_content($string) { 
    // ----- remove HTML TAGs ----- 
    $string = preg_replace ('/<[^>]*>/', ' ', $string); 
    // ----- remove control characters ----- 
    $string = str_replace("\r", '', $string);
    $string = str_replace("\n", ' ', $string);
    $string = str_replace("\t", ' ', $string);
    // ----- remove multiple spaces ----- 
    $string = trim(preg_replace('/ {2,}/', ' ', $string));
    return $string; 
} 
/*query*/
$args = array(
    'post_type' => 'job_listing',
    'post_status'=>'publish',
    'meta_key' => array('_application','_company_name','_job_location','_company_tagline','_company_website','_company_twitter'),                  
    'meta_value' => 'Spa',                
    'meta_compare' => 'LIKE', 
    'nopaging' => true,
    'order' => 'ASC',  
    'orderby' => 'meta_value',
);
$location_select=array();
$post_query = new WP_Query( $args );
if($post_query->have_posts()):
                            while ( $post_query->have_posts() ) { ?>
                        <?php
                        $post_query->the_post();
                            $post_id=get_the_ID();
                                foreach (get_post_meta( $post_id ) as $value => $meta_name) {
                                     echo strip_tags_content($meta_name[0]);
                                 }                      
                            }
endif; wp_reset_query();