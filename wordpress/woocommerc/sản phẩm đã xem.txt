<?php
//viewed cache
add_action('wp_head', 'set_cookies_viewed');
function set_cookies_viewed()
{
    if (is_product()) {
    global $post;
    if (empty($_COOKIE['woocommerce_recently_viewed'])) { 
        $viewed_products = array();
    } else {
        $viewed_products = wp_parse_id_list((array) explode('|', wp_unslash($_COOKIE['woocommerce_recently_viewed']))); 
    }
    $keys = array_flip($viewed_products);
    if (isset($keys[$post->ID])) {
        unset($viewed_products[$keys[$post->ID]]);
    }
    $viewed_products[] = $post->ID;
    if (count($viewed_products) > 8) { //chỉ cho phép lưu tối đa 10 sản phẩm
        array_shift($viewed_products);
    }
    wc_setcookie('woocommerce_recently_viewed', implode('|', $viewed_products));
    }
}
//viewed short_code
function show_viewed_product()
{   ob_start();
    $viewed_products_string = (isset($_POST['viewed_cookie']))?esc_attr($_POST['viewed_cookie']) : '';
    $viewed_products=explode("%7C",$viewed_products_string);
    $viewed_products=array_reverse($viewed_products,true);
?>
<div id="recently_view">
    <div class="viewed-product-button">
        <i class="fa fa-angle-double-left"></i>
    </div>
    <div class="container">
        <?php
            if (!empty($viewed_products)) 
            { ?>
    <div class="swiper-viewed-title">Đã xem</div>
    <div class="swiper-viewed-products">
        <div class="viewed-products-swiper-button-prev swiper-button-prev"><i class="fas fa-chevron-left"></i></div>
        <div class="swiper-wrapper">
            <?php
            foreach($viewed_products as $product_id)
            {   $product=wc_get_product( $product_id );
                ?><div class="swiper-slide">
                    <?= wp_get_attachment_image($product->get_image_id(),'medium'); ?>
                    <h2 class="viewed-product-name"><?= $product->get_name(); ?></h2>
                    <?php if ( $price_html = $product->get_price_html() ) : ?>
	<span class="price"><?php echo $price_html; ?></span>
<?php endif; ?>
                  </div>
      <?php } ?>
        </div>
        <div class="viewed-products-swiper-button-next swiper-button-next"><i class="fas fa-chevron-right"></i></div>
    </div>
    <div class="swiper-viewed-products-mini">
        <div class="viewed-products-swiper-button-prev swiper-button-prev"><i class="fas fa-chevron-left"></i></div>
        <div class="swiper-wrapper">
            <?php
            foreach($viewed_products as $product_id)
            {   $product=wc_get_product( $product_id );
                ?><div class="swiper-slide">
                    <?= wp_get_attachment_image($product->get_image_id(),'medium'); ?>
                  </div>
      <?php } ?>
        </div>
        <div class="viewed-products-swiper-button-next swiper-button-next"><i class="fas fa-chevron-right"></i></div>
    </div>
        <?php }
        ?>
    </div>
</div>
<?php
    $result = ob_get_clean();
    wp_send_json_success($result); 
    die();
}
add_action( 'wp_ajax_show_viewed_product', 'show_viewed_product' );
add_action( 'wp_ajax_nopriv_show_viewed_product', 'show_viewed_product' );

function shortcode_viewed()
{ 
?>
<script src="https://dugarcocollection.com.vn/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6"></script>
<div id="ajax_viewed_product"class="none_viewed"></div>
<script type="text/javascript">
  jQuery(function($){
$(document).ready(function(){
      function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}
  var viewed_cookie=readCookie('woocommerce_recently_viewed');
  if(viewed_cookie!=null)
  {
      $.ajax({
                    type : "post", 
                    dataType : "json", 
                    url : '<?php echo admin_url('admin-ajax.php');?>', 
                    data : {
                        action: "show_viewed_product", 
                        viewed_cookie: viewed_cookie,
                    },
                    context: this,
                    beforeSend: function(){
                    },
                    success: function(response) {
                    $('#ajax_viewed_product').removeClass('none_viewed');
                    $('#ajax_viewed_product').html(response.data);
					$('#ajax_viewed_product').append('<div class="button-close-viewed"><i class="fas fa-times-circle"></i></div>');
      $('.swiper-viewed-products').css('display','none');
						var swiper_viewed_mini = new Swiper(".swiper-viewed-products-mini", {
		loop: false,
		direction: 'vertical',
		autoplay: false,
        slidesPerView: 3,
        spaceBetween: 0,
        grabCursor: true,
        navigation: {
        nextEl: '.viewed-products-swiper-button-next',
        prevEl: '.viewed-products-swiper-button-prev',
        },
     }); 
 
     $('.viewed-product-button').click(function()
     {
        if($(this).hasClass('is-open'))
        {
            $(this).removeClass('is-open');
            $('.swiper-viewed-products').css('display','none');
            $('#ajax_viewed_product').css('width','100px');
            $('.swiper-viewed-products-mini').css('display','block');
        }
        else
        {
            $(this).addClass('is-open');
            $('.swiper-viewed-products').css('display','block');
            $('#ajax_viewed_product').css('width','500px');
            $('.swiper-viewed-products-mini').css('display','none');
			var swiper_viewed = new Swiper(".swiper-viewed-products", {
		loop: false,
		autoplay: false,
        slidesPerView: 3,
        spaceBetween: 0,
        grabCursor: true,
        navigation: {
        nextEl: '.viewed-products-swiper-button-next',
        prevEl: '.viewed-products-swiper-button-prev',
        },
     });
        }
     });
    $('div#ajax_viewed_product').after('<div class="mini-button-viewed"><div class="mini-button-viewed-text">Đã xem</div><div class="mini-button-viewed-icon"><i class="fas fa-chevron-up"></i></div></div>');
	$('.button-close-viewed').click(function()
	{
		$('.mini-button-viewed').addClass('active');
		$('#ajax_viewed_product').addClass('none_viewed');
	});
    $('.mini-button-viewed').click(function()
	{
		$(this).removeClass('active');
		$('#ajax_viewed_product').removeClass('none_viewed');
	});
					},
                    error: function () {
      }
                });
  }
  });
})
</script>
<?php }
add_action('wp_head','shortcode_viewed');
