﻿
if (!function_exists('my_commonPriceHtml')) {

function my_commonPriceHtml($price_amt, $regular_price, $sale_price) {
    $html_price = '<p class="price" style="ttext-decoration: none;" >';
    //if product is in sale
    if (($price_amt == $sale_price) && ($sale_price != 0)) {
        $html_price .= '<ins style="text-decoration: none;" >' . wc_price($sale_price) . '</ins>';
    }
    //in sale but free
    else if (($price_amt == $sale_price) && ($sale_price == 0)) {
		  $html_price .= '<del style="ttext-decoration: none;" >' . wc_price($regular_price) . '</del>';
        $html_price .= '<ins style="ttext-decoration: none;" >Miễn phí</ins>';
    }
    //not is sale
    else if (($price_amt == $regular_price) && ($regular_price != 0)) {
        $html_price .= '<ins style="text-decoration: none;" >' . wc_price($regular_price) . '</ins>';
    }
    //for free product
    else if (($price_amt == $regular_price) && ($regular_price == 0)) {
        $html_price .= '<ins style="text-decoration: none;" >Miễn phí</ins>';
    }
    $html_price .= '</p>';
    return $html_price;
}
}

add_filter('woocommerce_get_price_html', 'my_simple_product_price_html', 100, 2);

function my_simple_product_price_html($price, $product) {
if ($product->is_type('simple')) {
    $regular_price = $product->regular_price;
    $sale_price = $product->sale_price;
    $price_amt = $product->price;
    return my_commonPriceHtml($price_amt, $regular_price, $sale_price);
} else {
    return $price;
}
}

add_filter('woocommerce_variation_sale_price_html', 'my_variable_product_price_html', 10, 2);
add_filter('woocommerce_variation_price_html', 'my_variable_product_price_html', 10, 2);

function my_variable_product_price_html($price, $variation) {
$variation_id = $variation->variation_id;
//creating the product object
$variable_product = new WC_Product($variation_id);

$regular_price = $variable_product->regular_price;
$sale_price = $variable_product->sale_price;
$price_amt = $variable_product->price;

 return my_commonPriceHtml($price_amt, $regular_price, $sale_price);
 }

add_filter('woocommerce_variable_sale_price_html', 'my_variable_product_minmax_price_html', 10, 2);
add_filter('woocommerce_variable_price_html', 'my_variable_product_minmax_price_html', 10, 2);

function my_variable_product_minmax_price_html($price, $product) {
$variation_min_price = $product->get_variation_price('min', true);
$variation_max_price = $product->get_variation_price('max', true);
$variation_min_regular_price = $product->get_variation_regular_price('min', true);
$variation_max_regular_price = $product->get_variation_regular_price('max', true);

if (($variation_min_price == $variation_min_regular_price) && ($variation_max_price == $variation_max_regular_price)) {
    $html_min_max_price = $price;
} else {
    $html_price = '<p class="price"  style="ttext-decoration: none;" >';
    $html_price .= '<del>' . wc_price($variation_min_regular_price) . '-' . wc_price($variation_max_regular_price) . 
    $html_price .= '<ins>' . wc_price($variation_min_price) . '-' . wc_price($variation_max_price) . '</ins>';'</del>';
    $html_min_max_price = $html_price;
}

return $html_min_max_price;
}