/*đếm vị trí chuỗi trong chuỗi*/
function strpos_all($string_in, $string_find) {
    $offset = 0;
    $allpos = array();
    while (($pos = strpos($string_in, $string_find, $offset)) !== FALSE) {
        $offset   = $pos + 1;
        $allpos[] = $pos;
    }
    return $allpos;
}
function my_added_page_content ( $content ) {
if(get_field('san_pham_chen_them_post')['danh_sach_san_pham'])
    {
    if(get_post_type()=='post' && count(get_field('san_pham_chen_them'))>0)
	{
		       $string_id="";
			   foreach(get_field('san_pham_chen_them') as $value)
				   {
					   $string_id.=$value->ID.',';
				   }
			   $string_id=substr($string_id, 0, -1);
			   $count_p= count(strpos_all(get_the_content(),'</p>'));
			   if($count_p>5)
			   {
				   $p_out=floor($count_p/2);
				   $p_length=strpos_all(get_the_content(),'</p>')[$p_out];
				   $cut_string=substr(get_the_content() , 0 , $p_length+4 );
				   $cut_string_new=$cut_string.'<br>[products columns="4" ids="'.$string_id.'"]<br>';
				   $the_content_new= str_replace($cut_string,$cut_string_new,get_the_content());
				   return $the_content_new;
			   }
			   else 
			   {  
				   $cut_string=get_the_content();
				   $cut_string_new=$cut_string.'<br>[products columns="4" ids="'.$string_id.'"]<br>';
				   $the_content_new= str_replace($cut_string,$cut_string_new,get_the_content());
				   return $the_content_new;
			   }
    }
}
    return $content;
}
add_filter( 'the_content', 'my_added_page_content');