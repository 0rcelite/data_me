﻿.bien-the form.variations_form.cart
{
    height: 200px;
}
.bien-the tr
{
    padding-top: 10px;
    padding-bottom: 10px;
    background-color: #e9e9e9;
}
.bien-the .woocommerce-variation-price
{
    height: 54px;
}
.bien-the td.label
{
    display: block!important;
   padding-left: 20px!important;
   min-width: 32%!important;
   padding-top: 10px!important;
   padding-bottom: 10px!important;
   color: black!important;
   font-family: 'montserrat' !important;
   background-color: #e9e9e9;
} 
.bien-the .reset_variations
{
   padding:5px 10px 5px 10px !important;
   font-family: 'montserrat' !important;
   font-size: 14px !important;
   font-weight: 600 !important;
   color: #ffffff !important;
  background-color: #CE6673;
   border-style: solid;
   border-width: 0px;
   border-radius: 5px;
}
.bien-the .woo-variation-items-wrapper
{
    background-color: #e9e9e9;
}
.bien-the span.woocommerce-Price-amount.amount
{
    font-family: 'montserrat';
    font-size: 26px;
    font-weight: 700;
    color: #CE6673;
     position: absolute!important;
    width: 100%!important;
    margin-top: -5px!important;  
    display: block!important;
    line-height: 46px!important;
    
    padding-bottom: 10px;
}
.bien-the span.woocommerce-Price-amount.amount::before
{
    content: 'Giá SP: ';
    font-family: 'montserrat';
    font-size: 15px;
    font-weight: 700;
    color: black;
    padding-left: 20px;
    padding-bottom: 20px;
      display: block;
    color: black!important;
    width: 30%!important;
    float: left;
}


.bien-the .quantity
{
    position: absolute!important;
    width: 100%!important;
    margin-top: -5px!important;  
    display: block!important;
    line-height: 20px!important;
}
.bien-the div.quantity::before
{
    content: 'Số lượng: ';
    font-family: ''montserrat'';
    font-size: 16px;
    font-weight: 700;
    display: block;
    color: black!important;
    width: 30%!important;
    padding-left: 20px;
    padding-top: 10px;
    float: left;
}

.bien-the button.single_add_to_cart_button.button.alt
{
    width: 100%!important;
    margin-top: 55px!important;
}
.bien-the button.button.buy_now_button
{
       position: absolute!important;
    width: 100%!important;
    margin-top: 130px!important;
    color: #ffffff!important;
}
.bien-the .woocommerce #respond input#submit.disabled, .woocommerce #respond input#submit:disabled, .woocommerce #respond input#submit:disabled[disabled], .woocommerce a.button.disabled, .woocommerce a.button:disabled, .woocommerce a.button:disabled[disabled], .woocommerce button.button.disabled, .woocommerce button.button:disabled, .woocommerce button.button:disabled[disabled], .woocommerce input.button.disabled, .woocommerce input.button:disabled, .woocommerce input.button:disabled[disabled]
{
  opacity: 50;
}

.bien-the button.single_add_to_cart_button.button.alt::before
{
     font-family: "Font Awesome 5 Free"; font-weight: 900; content: "\f217";
    font-size: 16px;
     opacity: 100;
     background-color: #ffffff00;
     position: relative;
     padding-right: 10px;

}
.bien-the li.variable-item.button-variable-item

{margin-right:20px !important;}

.bien-the button
{
    margin-left: 0px!important;
}
.bien-the .woo-selected-variation-item-name
{
    opacity: 0!important;
}