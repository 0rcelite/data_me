/*thêm phân loại sp chờ cập nhật ảnh*/
function wpdocs_custom_post_status(){
    register_post_status( 'new_api_nhanh', array(
        'label'                     => _x( 'Mới đồng bộ', 'product' ),
        'public'                    => false,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Mới đồng bộ <span class="count">(%s)</span>', 'Mới đồng bộ <span class="count">(%s)</span>' ),
    ) );
}
add_action( 'init', 'wpdocs_custom_post_status' );
// Using jQuery to add it to post status dropdown
add_action('admin_footer-post.php', 'wpb_append_post_status_list');
function wpb_append_post_status_list(){
global $post;
$complete = '';
$label = '';
if($post->post_type == 'product'){
if($post->post_status == 'new_api_nhanh'){
$complete = ' selected="selected"';
$label = '<span id="post-status-display"> Mới đồng bộ</span>';
}
echo '
<script>
jQuery(document).ready(function($){
$("select#post_status").append("<option value=\"new_api_nhanh\" '.$complete.'>Mới đồng bộ</option>");
$(".misc-pub-section label").append("'.$label.'");
});
</script>
';
}
}
// Using jQuery to add it to post status dropdown in quick edit
add_action('admin_footer-edit.php','rudr_status_into_inline_edit');
function rudr_status_into_inline_edit() { 
	echo "<script>
	jQuery(document).ready( function() {
		jQuery( 'select[name=\"_status\"]' ).append( '<option value=\"new_api_nhanh\">Mới đồng bộ</option>' );
	});
	</script>";
}